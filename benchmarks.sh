#!/bin/bash
pushd bench_results/
dat_files=`ls | cut -d "." -f 1`
popd

mkdir -p tmp


for name in $dat_files; do
/usr/bin/gnuplot <<-EOF
    set autoscale
    set terminal png
    set key autotitle columnheader
    set output "tmp/$name.png"
    set ylabel "time (ms)"
    set xlabel "Vertices"
    stats "bench_results/$name.dat"
    cols = STATS_columns
    plot for [i=2:cols] "bench_results/$name.dat" using i with linespoints title columnheader(i)
EOF
done

