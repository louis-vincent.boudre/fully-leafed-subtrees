//! Number theory related module

use rand::{thread_rng, Rng};
use std::collections::{HashSet};

/// Random path between divisible partition
pub fn ranpdi(partition: &Vec<&[u32]>) -> Vec<usize> {
    let mut rng = thread_rng();
    let partition_lens: Vec<usize> = partition.iter().map(|list| list.len()).collect();
    let mut path = vec![rng.gen_range(0, partition.len())];
    let mut i = path[0];
    while path.len() < partition.len() {
        let j = rng.gen_range(0, partition.len());
        if i == j {
            continue;
        }
        let a = &partition_lens[i];
        let b = &partition_lens[j];
        if a % b == 0 || b % a == 0 {
            i = j;
            path.push(j);
        }
    }
    return path;
}

/// Random k-subset of an n-Set
pub fn ranksb(n: usize, k: usize) -> Vec<usize> {
    let mut rng = thread_rng();
    let mut generated = HashSet::with_capacity(n);
    let mut result = vec![];
    while result.len() < k {
        let r = rng.gen_range(1,n);
        if !generated.contains(&r) {
            result.push(r);
            generated.insert(r);
        }
    }
    result.sort_unstable();
    return result;
}

/// Random composition of n in k parts
pub fn rancom(n: usize, k: usize) -> Vec<usize> {
    let mut out = ranksb(n+k-1, k-1);
    let mut l = 0;
    out.push(n+k);
    for i in 0..k {
        let m = out[i];
        out[i] = m-l-1;
        l=m;
    }
    out.sort_unstable();
    let mut i = 0;
    let mut last = out.len() - 1;
    while out[i] == 0 && i < last {
        if out[last] > 1 {
            out[last] -= 1;
            out[i] = 1;
            i += 1;
        } else {
            last -= 1;
        }
    }
    return out;
}

#[cfg(test)]
mod tests {
    use number::*;

    #[test]
    fn test_rancom() {
        let mut rng = thread_rng();
        for i in 0..100 {  // 100 tests
            let n = rng.gen_range(5, 1000);
            let k;
            if n < 10 {
                k = rng.gen_range(2,4);
            } else {
                k = rng.gen_range(5, 10);
            }
            let actual = rancom(n,k);
            assert_eq!(n, actual.iter().sum());
        }
    }
}