use std::thread::{JoinHandle};
use std::thread;
use std::sync::mpsc::{Sender, Receiver, SyncSender, sync_channel, channel, TrySendError, TryRecvError};
use std::sync::{Mutex, Arc};
use std::sync::atomic::{AtomicUsize, Ordering};
use std::collections::{VecDeque};


trait FnBox {
    fn call_box(self: Box<Self>);
}

impl<F: FnOnce()> FnBox for F {
    fn call_box(self: Box<F>) {
        (*self)()
    }
}

type Job = Box<FnBox + Send + 'static>;

enum Message {
    NewJob(Job),
    Terminate
}

struct ThreadPool {
    workers: Vec<Worker>,
    sender: Sender<Message>,
    available_workers: Arc<AtomicUsize>,
}

struct Worker {
    id: usize,
    thread: Option<JoinHandle<()>>
}

impl ThreadPool {
    fn new(size: usize) -> ThreadPool {
        let (sender, receiver) = channel();
        let receiver = Arc::new(Mutex::new(receiver));
        let mut workers = Vec::with_capacity(size as usize);
        let available_workers = Arc::new(AtomicUsize::new(size));
        for id in 0..size {
            workers.push(Worker::new(id, Arc::clone(&receiver), Arc::clone(&available_workers)));
        }
        ThreadPool {
            workers,
            sender,
            available_workers,
        }
    }

    fn execute<F>(&self, f: F)
        where F: FnOnce() + Send + 'static 
    {
        self.available_workers.fetch_sub(1, Ordering::SeqCst);
        let job = Box::new(f);
        self.sender.send(Message::NewJob(job)).expect("Error: disconnected from thread pool channel.");
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        for _ in &mut self.workers {
            self.sender.send(Message::Terminate).unwrap();
        }

        for worker in &mut self.workers {
            if let Some(thread) = worker.thread.take() {
                thread.join().unwrap();
            }
        }
    }
}

impl Worker {
    fn new(id: usize, rx: Arc<Mutex<Receiver<Message>>>, counter: Arc<AtomicUsize>) -> Worker {
        let thread = thread::spawn(move || {
            loop {
                let message = rx.lock().unwrap().recv().unwrap();
                match message {
                    Message::Terminate => {
                        break;
                    }
                    Message::NewJob(job) => {
                        job.call_box();
                        counter.fetch_add(1, Ordering::SeqCst);
                    }
                }
            }
        });
        Worker {
            id: id,
            thread: Some(thread),
        }
    }
}

/// An interface to manage mutlithreaded branch-and-bound algorithm.
/// This structure needs a goal to achieve and a unit of work to complete the goal.
pub struct GoalSolver<Goal, Unit> {
    thread_pool: ThreadPool,
    max_worker: usize,
    /// The current state of the goal to solve.
    goal: Arc<Goal>,
    /// Works waiting to be acquire by a thread.
    pending_works: VecDeque<Unit>,
    /// Where the work comes from
    works_channel: (SyncSender<Unit>, Receiver<Unit>),
    /// Available free spaces in the pending works queue (similar mecanism to a semaphore).
    free_work_places: Arc<AtomicUsize>,
}

/// Sends work to the master thread.
pub struct WorkDispatcher<Unit> {
    work_sender: SyncSender<Unit>,
    free_work_places: Arc<AtomicUsize>,
}

/// WorkTransaction type, represents a work being dispatched.
/// The dispatch might failed, this structure is used as a "guard".
pub struct WorkTransaction<Unit> {
    lock: Arc<AtomicUsize>,
    consumed: bool,
    sender: SyncSender<Unit>,
}

impl <Unit> Drop for WorkTransaction<Unit> {
    fn drop(&mut self) {
        if !self.consumed {
            self.lock.fetch_add(1, Ordering::Relaxed);
        }
    }
}

impl <Unit> WorkTransaction<Unit> {
    /// Sends the work to the master thread.
    /// 
    /// Returns true if work has been send successfully.
    pub fn send(mut self, work: Unit) -> bool {
        let result;
        match self.sender.try_send(work) {
            Err(variant) => {
                match variant {
                    TrySendError::Full(_) => { 
                        self.lock.fetch_add(1, Ordering::Relaxed);
                        result = false 
                    },
                    TrySendError::Disconnected(_) => panic!("Error, disconnected"),
                }
            }
            _ => result = true,
        }
        self.consumed = true;
        return result;
    }
}

impl <Unit> WorkDispatcher<Unit> {
    /// Try to reserve an empty place in the pending works.
    pub fn try_acquire(&self) -> Option<WorkTransaction<Unit>> {
        let mut curr = self.free_work_places.load(Ordering::Relaxed);
        loop {
            if curr == 0 {
                return None;
            }
            match self.free_work_places.compare_exchange_weak(curr, curr - 1, Ordering::SeqCst, Ordering::Relaxed) {
                Ok(_) => { 
                    return Some(WorkTransaction {
                        lock: self.free_work_places.clone(),
                        consumed: false,
                        sender: self.work_sender.clone(),
                    });
                },
                Err(actual) => curr = actual,
            }
        }
    }
}

impl <Goal, Unit> GoalSolver<Goal, Unit>
    where Goal: Sync + Send + 'static,
          Unit: Send + Clone + 'static,
{
    
    pub fn new(mut max_worker: usize, goal: Goal, initial_unit: Unit) -> GoalSolver<Goal, Unit>
    {   
        let mut pending_works = VecDeque::new();
        pending_works.push_back(initial_unit);
        let mut free_work_places = 0;
        if max_worker <= 1 {
            max_worker = 0;
        } else {
            max_worker += 4;
            free_work_places = max_worker - 1;  // minus one because we pushed the initial_unit in the queue.
        }
        
        GoalSolver {
            thread_pool: ThreadPool::new(max_worker),
            max_worker,
            goal: Arc::new(goal),
            pending_works,
            works_channel :sync_channel(max_worker),
            free_work_places: Arc::new(AtomicUsize::new(free_work_places)),
        }
    }

    /// Builds a pre-configured work dispatcher
    fn work_dispatcher_factory(&mut self) -> WorkDispatcher<Unit> {
        WorkDispatcher {
            work_sender: self.works_channel.0.clone(),
            free_work_places: Arc::clone(&self.free_work_places),
        }
    }


    pub fn solve<F>(mut self, routine: F) -> Goal
        where F: Fn(Arc<Goal>, Unit, WorkDispatcher<Unit>) -> () + Send + Sync + 'static,
    {   
        // Nice fallback for no threaded environment
        if self.max_worker == 0 {
            let work = self.pending_works.pop_front().unwrap();
            let goal = self.goal.clone();
            let wd = self.work_dispatcher_factory();
            routine(goal, work, wd);
        } else {
            let routine = Arc::new(routine);
            loop {
                if self.thread_pool.available_workers.load(Ordering::SeqCst) == self.max_worker && 
                   self.pending_works.is_empty() 
                {
                    self.drain_works();
                    if self.pending_works.is_empty() {
                        break;
                    }
                    continue;
                }
                self.fetch_works(3);
                self.try_dispatching_work(5, Arc::clone(&routine));
            }
        }
        let mut goal = self.goal;
        loop {
            match Arc::try_unwrap(goal) {
                Ok(inner) => return inner,
                Err(arc) => goal = arc,
            }
        }
    }   

    fn drain_works(&mut self) {
        loop {
            if self.pending_works.len() == self.max_worker {
                break;
            }
            match self.works_channel.1.try_recv() {
                Ok(work) => {
                    self.pending_works.push_back(work);
                },
                Err(err) => {
                    match err {
                        TryRecvError::Empty => break,
                        TryRecvError::Disconnected => {
                            panic!("Disconnect from works channel.");
                        }
                    }
                },
            }
        }
    }
    
    fn fetch_works(&mut self, max_works: usize) {
        for _i in 0..max_works {
            if self.pending_works.len() == self.max_worker {
                return;
            }
            match self.works_channel.1.try_recv() {
                Ok(work) => {
                    self.pending_works.push_back(work);
                },
                Err(err) => {
                    match err {
                        TryRecvError::Empty => break,
                        TryRecvError::Disconnected => {
                            panic!("Disconnect from works channel.");
                        }
                    }
                },
            }
        }
    }
    
    fn try_dispatching_work<F>(&mut self,  max_works: usize, routine: Arc<F>)
        where F: Fn(Arc<Goal>, Unit, WorkDispatcher<Unit>) -> () + Send + Sync + 'static,
    {
        for _i in 0..max_works {
            if self.thread_pool.available_workers.load(Ordering::SeqCst) == 0 {
                return;
            }
            match self.pending_works.pop_front() {
                Some(work) => {
                    self.free_work_places.fetch_add(1, Ordering::Relaxed);
                    let th_goal = self.goal.clone();
                    let wd = self.work_dispatcher_factory();
                    let th_routine = routine.clone();
                    self.thread_pool.execute(move || {
                        th_routine(th_goal, work, wd);
                    });
                },
                None => break,
            }
        }
    }
}
