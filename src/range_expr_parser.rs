//! Range expression parser module
use std::ops::Bound;
extern crate regex;
use self::regex::{Regex};

/// Mixed of Range, RangeFrom, RangeFull, RangeTo and fixed value.
pub type MixedRange = (Bound<usize>, Bound<usize>);

/// Default value, represents the absence of `range`
pub fn no_range() -> MixedRange {
    (Bound::Unbounded, Bound::Excluded(0))
}

/// Converts a range expression to an actual tuple of bounds.
/// 
/// # Examples
/// 
/// ```    
/// use range_expr_parser::{to_range}
/// use std::ops::Bound::*;
/// 
/// let range_expr = "10..50".to_owned();
/// assert_eq!((Included(10), Excluded(50)), to_range(&range_expr).ok().unwrap());
/// ```
/// 
pub fn to_range(expr: &String) -> Result<MixedRange, &'static str> {
    let range_regex = Regex::new(r"^(?P<single>\d+$)|(?P<start>\d+)?..(?P<end>\d+)?$").unwrap();
    if !range_regex.is_match(expr) {
        return Err("invalid range, format must be: `a..b`, `..b`, `a..`, `a`, `..` where b is exclusive");
    }
    if expr == ".." {
        return Ok((Bound::Unbounded, Bound::Unbounded));
    }
    let caps = range_regex.captures(expr).unwrap();
    let start: usize;
    let end: usize;
    match caps.name("single") {
        Some(s) => {
            start = s.as_str().parse().unwrap();
            end   = start + 1;
            return Ok((Bound::Included(start), Bound::Excluded(end)));
        },
        _ => (),
    }
    if caps.name("start").is_some() && caps.name("end").is_some() {
        start = caps.name("start").unwrap().as_str().parse().unwrap();
        end   = caps.name("end").unwrap().as_str().parse().unwrap();
        return Ok((Bound::Included(start), Bound::Excluded(end)));
    }
    if caps.name("start").is_none() {
        end = caps.name("end").unwrap().as_str().parse().unwrap();
        return Ok((Bound::Unbounded, Bound::Excluded(end)));
    }
    start = caps.name("start").unwrap().as_str().parse().unwrap();
    return Ok((Bound::Included(start), Bound::Unbounded));
}


#[cfg(test)]
mod tests {
    use range_expr_parser::*;
    use std::ops::Bound::*;
    #[test]
    fn test_range_between_0_and_10() {
        let actual = to_range(&"0..10".to_owned()).ok().unwrap();
        assert_eq!((Included(0), Excluded(10)), actual);
    }

    #[test]
    fn test_range_to_10() {
        let actual = to_range(&"..10".to_owned()).ok().unwrap();
        assert_eq!((Unbounded, Excluded(10)), actual);
    }

    #[test]
    fn test_range_from_10() {
        let actual = to_range(&"10..".to_owned()).ok().unwrap();
        assert_eq!((Included(10), Unbounded), actual);
    }

    #[test]
    fn test_range_full() {
        let actual = to_range(&"..".to_owned()).ok().unwrap();
        assert_eq!((Unbounded, Unbounded), actual);
    }

    #[test]
    fn test_range_fixed() {
        let actual = to_range(&"1".to_owned()).ok().unwrap();
        assert_eq!((Included(1), Excluded(2)), actual);
    }
}