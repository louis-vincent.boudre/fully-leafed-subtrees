use graph::*;
use std::process::{Command, Stdio};
use std::error::Error;

extern crate regex;
use self::regex::Regex;
use std::sync::{Arc};
use graph::{Permutation};

pub struct SageMath;

impl SageMath {
    ///
    /// Convert each group element from string to a circular sequences of permutations.Child
    /// 
    /// Examples
    /// [(1,2)(4,5,6,7), (3,4)] => [ [ [1,2], [4,5,6,7] ], [ [3,4] ] ]
    #[allow(dead_code)]
    fn to_vertex_permutation(group: Vec<String>) -> Vec<Arc<Permutation>> {
        let re = Regex::new(r"\(\d+(,\d+)*\)").unwrap();
        let digits = Regex::new(r"\d+").unwrap();

        return group.iter().map(|g_str| {
            return Permutation::new(re.captures_iter(g_str).map(|caps| {
                return digits.captures_iter(&caps[0])
                    .map(|d| *(&d[0].parse::<u32>().unwrap()))
                    .collect();
            }).collect());
        })
        .map(|perm| Arc::new(perm))
        .collect();
    }

    /**
     * Captures all group element (strings) of the automorphism group.
     */
    #[allow(dead_code)]
    fn capture_permutations(sage_raw_perm_group_str: &String) -> Vec<String> {
        let re_group_element = Regex::new(r"(\(\d+(,\d+)*\))+").unwrap();

        return re_group_element.captures_iter(sage_raw_perm_group_str)
            .map(|caps| String::from(&caps[0])).collect();
    }

    /**
     * Retrieve the automorphism group of a given graph.
     */
    #[allow(dead_code)]
    pub fn automorphism_group(g: &Graph) -> Vec<Arc<Permutation>> {
        let declare_edges_cmd = format!("g = Graph({:?});", g.edges());
        let automorphism_group_cmd = "print(g.automorphism_group());";
        let cmds = [declare_edges_cmd.to_owned(), automorphism_group_cmd.to_owned()].join(" ");
        let process = match Command::new("sage")
                .arg("-c")
                .arg(cmds)
                .stdout(Stdio::piped())
                .spawn() {
                    Err(why) => panic!("couldn't spawn sage: {}", why.description()),
                    Ok(process) => process
                };
        let raw_output = process
            .wait_with_output()
            .expect("Failed to wait on child");

        let output = String::from_utf8(raw_output.stdout).unwrap();
        println!("output: {:?}", output);   
        let result = SageMath::to_vertex_permutation(SageMath::capture_permutations(&output));
        return result;
    }
}