//! Utility functions for various purposes

use var_chunks::{VariableChunks};
use rand::{thread_rng, Rng};

/// Slices the array into specified chunk sizes.
pub fn chunks_from<T: Clone>(data: &Vec<T>, sizes: Vec<usize>) -> VariableChunks<T> {
    VariableChunks::new(data, sizes)
}

/// Determines the minimum and maximum between two objects
/// and returns a tuple : (min, max).
pub fn min_max<T: Ord>(a: T, b: T) -> (T, T) {
    use std::cmp::Ordering;
    match a.cmp(&b) {
        Ordering::Less | Ordering::Equal => (a,b),
        Ordering::Greater => (b,a),
    }
}

/// Pick 2 random object from a given universe.
pub fn pick2<T: Clone>(universe: &Vec<T>) -> (T,T) {
    let mut rng = thread_rng();
    let i = rng.gen_range(0, universe.len());
    let j = rng.gen_range(0, universe.len());
    (universe[i].clone(), universe[j].clone())
}