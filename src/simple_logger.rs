extern crate time;

use log::{Log,LevelFilter,Metadata,Record,SetLoggerError};

struct SimpleLogger {
    level_filter: LevelFilter
}

impl Log for SimpleLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= self.level_filter
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!(
                "{} {:<5} [{}] {}",
                time::strftime("%Y-%m-%d %H:%M:%S", &time::now()).unwrap(),
                record.level().to_string(),
                record.module_path().unwrap_or_default(),
                record.args());
        }
    }

    fn flush(&self) {
    }
}

pub fn init(level_filter: LevelFilter) -> Result<(), SetLoggerError> {
    let logger = SimpleLogger { level_filter: level_filter };
    log::set_boxed_logger(Box::new(logger))
        .map(|()| log::set_max_level(level_filter))
}