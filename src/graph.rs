use std::collections::{BTreeSet, VecDeque, HashSet};
use std::convert::From;
use std::sync::Arc;
use number::*;
use utility::*;

use rand::{thread_rng, Rng};
use sagemath::{SageMath};

#[derive(Debug, Clone)]
pub struct Graph {
    nodes: Vec<Vec<u32>>,
    autormphism_generators: Vec<Arc<Permutation>>,  // generators
    orbits: Vec<Arc<Vec<u32>>>,
    stabilizers: Vec<Arc<Permutation>>,
    transversals: Vec<Arc<Permutation>> // Using Atomic reference counting ~= Schreier vector
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Permutation {
    permutation: Vec<Vec<u32>>
}

pub type Pattern = Vec<u32>;

impl Permutation {
    pub fn new(p: Vec<Vec<u32>>) -> Permutation {
        Permutation {
            permutation: p,
        }
    }

    pub fn apply(&self, v: u32) -> u32 {
        let mut w = v;
        // a permutation is represented by a set of
        // tuples (vec) with (possibly) different sizes.
        'outer: for i in 0..self.permutation.len() {
            let tuple = &self.permutation[i];
            for j in 0..tuple.len() {
                if tuple[j] == v {
                    w = tuple[(j+1) % tuple.len()];
                    break 'outer;
                }
            }
        }   
        return w;
    }

    pub fn apply_from(&self, vs: &Vec<u32>) -> Vec<u32> {
        vs.iter().map(|v| self.apply(*v)).collect()
    }
}

impl From<Vec<(u32, u32)>> for Graph {
    fn from(edges: Vec<(u32,u32)>) -> Graph {
        use std::cmp;
        if edges.len() == 0 {
            panic!("cannot build a graph from 0 edges");
        }
        let max = edges.iter().map(|(u,v)| cmp::max(u,v)).max().unwrap();
        let mut graph = Graph::basic_graph(max+1);
        for (u,v) in edges {
            graph.add_edge(u,v);
        }
        graph
    }
}

impl Graph {
    #[allow(dead_code)]
    pub fn new() -> Graph {
        Graph { 
            nodes: vec![], 
            autormphism_generators: vec![],
            orbits: vec![],
            stabilizers: vec![],
            transversals: vec![]
        }
    } 

    pub fn autormphism_generators(&self) -> &Vec<Arc<Permutation>> {
        return &self.autormphism_generators;
    }

    pub fn calculate_aut_gr(&mut self) {
        if self.autormphism_generators.len() > 0 {
            return;
        }
        self.autormphism_generators = SageMath::automorphism_group(self);
    }

    #[allow(dead_code)]
    pub fn pre_calculate_orbits(&mut self) {
        let mut calculated = vec![false; self.nb_nodes()];
        let temp = Arc::new(vec![]);
        self.orbits = vec![Arc::clone(&temp); self.nb_nodes()];
        let temp2 = Arc::new(Permutation::new(vec![]));
        self.transversals = vec![Arc::clone(&temp2); self.nb_nodes()];
        for v in self.nodes().iter() {
            let v = *v as usize;
            if calculated[v] {
                continue;
            }
            let mut visited = vec![false; self.nb_nodes()];
            let mut queue = VecDeque::new();
            queue.push_back(v);
            visited[v] = true;
            while !queue.is_empty() {
                let delta = queue.pop_front().unwrap() as u32;
                for gen in self.autormphism_generators.iter() {
                    let image = gen.apply(delta) as usize;
                    if !visited[image] {
                        visited[image] = true;
                        queue.push_back(image);
                        self.transversals[image] = Arc::clone(&gen);
                    }
                }
            }  
            let orbit_iterator = visited.iter()
                .enumerate()
                .filter(|(_, in_orbit)| **in_orbit)
                .map(|(i,_)| i as u32);

            // collect consume an iterator this is way we need to clone it.
            let orbit = orbit_iterator.clone()
                .collect();

            self.orbits[v] = Arc::new(orbit);
            orbit_iterator
                .map(|u| u as usize)
                .for_each(|omega| {
                    calculated[omega] = true;
                    if omega != v {
                        self.orbits[omega] = Arc::clone(&self.orbits[v]);
                    }
                });
        }
    }

    #[allow(dead_code)]
    pub fn orbit_of(&self, v: u32) -> &Vec<u32> {
        return &self.orbits[v as usize];
    }

    pub fn orbit_from(&self, vs: &Vec<u32>) -> Vec<Pattern> {
        let gens = self.autormphism_generators();
        let mut orbit = HashSet::new();
        let mut queue = VecDeque::new();
        queue.push_back(vs.to_vec());
        orbit.insert(vs.to_vec());
        while !queue.is_empty() {
            let delta = queue.pop_front().unwrap();
            for gen in gens.iter() {
                let mut gamma = gen.apply_from(&delta);
                let len = gamma.len();
                if len == 0 {
                    continue;
                }
                if gamma[len-1] < gamma[0] {
                    gamma.reverse();
                }
                if !orbit.contains(&gamma) {
                    orbit.insert(gamma.to_vec());
                    queue.push_back(gamma);
                }
            }
        }

        return orbit.iter().map(|e| e.to_vec()).collect();
    }

    pub fn basic_graph(n: u32) -> Graph {
        Graph { 
            nodes: vec![vec![]; n as usize],
            autormphism_generators: vec![],
            orbits: Vec::with_capacity(n as usize),
            stabilizers: Vec::with_capacity(n as usize),
            transversals: Vec::with_capacity(n as usize)
        }
    }

    pub fn complete_graph(n: u32) -> Graph {
        //let mut g:L = Graph::basic_graph(n);
        let mut g = Graph::basic_graph(n);
        for i in 0..n-1 {
            for j in i+1..n {
                g.add_edge(i,j);
            }
        }
        g
    }

    pub fn flower_snark(n: u32) -> Graph {
        let m = 4*n;
        let mut g = Graph::basic_graph(m);
        // Visit https://en.wikipedia.org/wiki/Flower_snark for more info.
        // 4 star graph on 4 vertices
        for i in 0..n {
            for j in i*4..i*4+1 {
                for k in i*4+1..i*4+4 {
                    g.add_edge(j, k);
                }
            } 
        }
        // connect each star graph in a circular pattern
        for i in 0..m {
            if i % 4 == 0 {
                continue;
            }
            g.add_edge(i, (i+4) % m);
        }
        g
    }

    pub fn cycle_graph(n: u32) -> Graph {
        let mut g = Graph::basic_graph(n);
        for i in 0..n {
            let mut j = i + 1;
            if i == n - 1 {
                j = 0;
            }
            g.add_edge(i,j);
        } 
        g
    }
    
    pub fn complete_bipartite_graph(m: u32, n: u32) -> Graph {
        let mut g = Graph::basic_graph(m+n);
        for i in 0..m {
            for j in m..(m+n) {
                g.add_edge(i, j);
            }
        } 
        g
    }

    pub fn hoffman_singleton() -> Graph {
        let mut graph = Graph::basic_graph(50);
        let mut pentagons = vec![];
        let pentagrams: Vec<Vec<u32>>;
        for i in 0..5 {
            let pentagon_vertices = [i, (i+5)%25, (i+10)%25, (i+15)%25, (i+20)%25];
            for j in 0..5 {
                graph.add_edge(pentagon_vertices[j], pentagon_vertices[(j+1)%5]);
            }
            pentagons.push(pentagon_vertices);
        }

        pentagrams = pentagons.iter().map(|p| p.iter().map(|v| v + 25).collect()).collect();
        pentagrams.iter().for_each(|p: &Vec<u32>| {
            for i in 0..p.len() {
                graph.add_edge(p[i], p[(i+2)%5]);
                graph.add_edge(p[i], p[(i+3)%5]);
            }
        });

        pentagons.iter().enumerate().for_each(|(h,pentagon)| {
            pentagon.iter().enumerate().for_each(|(j, v)| {
                pentagrams.iter().enumerate().for_each(|(i, pentagram)| {
                    graph.add_edge(*v, pentagram[(h*i+j)%5])       
                });
            });
        });

        graph
    }

    pub fn wheel_graph(n: u32) -> Graph {
        let mut g = Graph::cycle_graph(n);
        let k = n;
        g.nodes.push(vec![]);
        for i in 0..n {
            g.add_edge(k, i);
        }
        g
    }
    
    pub fn friendship_graph(n: u32) -> Graph {
        let mut graph = Graph::basic_graph(n*2+1);
        for i in 0..2*n {
            graph.add_edge(i, 2*n);
            if i % 2 == 0 {
                graph.add_edge(i, i+1);
            }
        }
        graph
    }

    pub fn generalized_peterson(n: u32, k: u32) -> Graph {
        if k >= n/2 {
            panic!("k must be < n/2 to create a generalized peterson graph");
        }
        let mut graph = Graph::basic_graph(n*2);
        let us: Vec<u32> = (0..n).collect();
        let vs: Vec<u32> = (n..2*n).collect();
        for i in 0..n {
            graph.add_edge(us[i as usize], us[((i+1)%n) as usize]);
            graph.add_edge(us[i as usize], vs[i as usize]);
            graph.add_edge(vs[i as usize], vs[((i+k)%n) as usize]);
        }
        graph
    }

    pub fn hyper_cube(dim: u32) -> Graph {
        let mut cube_graph = Graph::basic_graph(2u32.pow(dim));
        Graph::hyper_cube2(dim, 0, &mut cube_graph);
        return cube_graph;
    }

    fn hyper_cube2(dim: u32, at: u32, graph: &mut Graph) {
        assert!(dim > 1);
        let k = 2u32.pow(dim);
        if dim == 2 {
            for i in at..at+k {
                let mut j = i + 1;
                if j % 4 == 0 {
                    j = i - (i % 4);
                }
                graph.add_edge(i, j);
            }
            return;
        }
        let n = k/2;
        Graph::hyper_cube2(dim-1, at, graph);
        Graph::hyper_cube2(dim-1, at+n, graph);

        for i in at..at+n {
            let j = i + n;
            graph.add_edge(i,j);
        }
    }

    /// Calcultes the number of edges from a density [0.2;1]
    /// The minimum density must be 0.2 since we only want connected graph. Otherwise
    /// They would not be conncted.
    fn edges_from_density(n: u32, density: f32) -> u32 {
        assert!(n > 0 && density >= 0.2);
        (density/ 2.0 * (n * (n-1)) as f32).floor() as u32
    }

    /// The length of a shortest cycle contained in the graph
    #[allow(dead_code)]
    pub fn girth(&self) -> u32 {
        use std::cmp::{min};
        let mut smallest_cycle = <u32>::max_value();
        for v in self.nodes() {
            let mut parents = vec![<u32>::max_value(); self.nb_nodes()];
            let mut queue = VecDeque::new();
            let mut visited = HashSet::new();
            let mut dist = vec![0; self.nb_nodes()];
            queue.push_back(v);
            while !queue.is_empty() {
                let x = queue.pop_front().unwrap();
                visited.insert(x);
                
                for y in self.neighbors(x).unwrap().iter() {
                    if *y == parents[x as usize] {
                        continue;
                    }
                    if !visited.contains(y) {
                        parents[*y as usize] = x;
                        dist[*y as usize] = dist[x as usize] + 1;
                        queue.push_back(*y);
                    } else {
                        smallest_cycle = min(smallest_cycle, dist[x as usize] + dist[*y as usize] + 1);
                    }
                }
            }
        }
        smallest_cycle
    }

    #[allow(dead_code)]
    pub fn random_graph(n: u32, density: f32) -> Graph {
        let mut g = Graph::basic_graph(n);
        let mut remaining_edge = Graph::edges_from_density(n, density) as i32;
        let mut rng = thread_rng();
        for i in 0..n {
            if i == 0 {
                continue;
            }
            let j: u32 = rng.gen_range(0, i);
            g.add_edge(i, j);
            remaining_edge -= 1;
        }
        while remaining_edge > 0 {
            let i = rng.gen_range(0,n);
            let j = rng.gen_range(0,n);
            if i == j || g.has_edge(i, j) {
                continue;
            }
            g.add_edge(i, j);
            remaining_edge -= 1;
        }
        g
    }

    pub fn nb_nodes(&self) -> usize {
        self.nodes.len()
    }

    #[allow(dead_code)]
    pub fn nb_edges(&self) -> usize {
        self.edges().len()
        //self.nodes.iter().fold(0, |acc, ns| acc + ns.len()) / 2 
    }

    /// Depth first search
    fn dfs(&self, node: u32, visited: &mut BTreeSet<u32>) {
        if visited.contains(&node) {
            return;
        }
        visited.insert(node.clone());
        if let Some(neighbors) = self.neighbors(node) {
            for neighbor in neighbors {
                self.dfs(*neighbor, visited);
            }
        }
    }

    #[allow(dead_code)]
    pub fn is_connected(&self) -> bool {
        let mut visited = BTreeSet::new();
  
        if self.nodes.len() > 0 {
            self.dfs(0, &mut visited);
            return visited.len() == self.nodes.len();
        }
        return false;    // null graphs
    }

    fn add_edge(&mut self, v1: u32, v2: u32) {
        if self.has_edge(v1, v2) {
            return;
        }
        self.nodes[v1 as usize].push(v2);
        self.nodes[v2 as usize].push(v1);
    }


    #[allow(dead_code)]
    pub fn nodes(&self) -> Vec<u32> {
        (0..self.nodes.len()).map(|n| n as u32).collect()
    }

    #[allow(dead_code)]
    pub fn neighbors(&self, v: u32) -> Option< &Vec<u32> > {
        assert!(v < (self.nodes.len() as u32));
        self.nodes.get(v as usize)
    }
    
    #[allow(dead_code)]
    pub fn has_vertex(&self, v1: u32) -> bool {
        v1 < (self.nodes.len() as u32)
    }
    
    #[allow(dead_code)]
    pub fn has_edge(&self, v1: u32, v2: u32) -> bool {
        self.nodes[v1 as usize].contains(&v2)
    }

    pub fn edges(&self) -> Vec<(u32,u32)> {
        c![ (v1.clone(), (*v2).clone()), 
            for v2 in self.neighbors(v1).unwrap(), 
            for v1 in self.nodes(), 
            if v1 < *v2 ]
    }
    
    pub fn random_sym_graph(n: u32, density: f32) -> Graph {
        let mut rng = thread_rng();
        let mut graph = Graph::basic_graph(n);
        let mut k = 0;
        let mut partition = vec![];
        let mut is_valid = false;
        while !is_valid {
            k = rng.gen_range(1, n);
            partition = rancom(n as usize, k as usize);
            is_valid = partition.iter().enumerate()
                .all(|(i,a)| {
                    partition.iter().enumerate()
                        .any(|(j,b)| j != i && (*b % a == 0 || a % *b == 0))
                });
        }
        
        let nodes = graph.nodes();
        let graph_partition: Vec<&[u32]>  = chunks_from(&nodes, partition.to_vec()).collect();
        let mut nodes: Vec<(u32, &[u32])> = vec![];
        for slice in graph_partition.iter() {
            slice.iter().for_each(|v| nodes.push((*v, slice)));
        }

        let path = ranpdi(&graph_partition);
        for i in 0..(k-1) as usize {
            let j = i+1;
            let pi = &graph_partition[path[i]];
            let pj = &graph_partition[path[j]];
            let (mut min, mut max) = (pi, pj);
            if pj.len() < pi.len() {
                min = pj;
                max = pi;
            }
            let quotient = max.len() / min.len();
            for w in 0..min.len() {
                let offset = 0;
                for r in 0..quotient {
                    graph.add_edge(max[(r+offset)%max.len()], min[w])
                }
            }
        }

        let mut remaining_edge = Graph::edges_from_density(n, density) as i32 - (graph.nb_edges() as i32);
        while remaining_edge > 0 {
            let ((v1, p1),(v2, p2)) = pick2(&nodes);
            if p1 == p2 {
                let (i, _) = p1.iter().enumerate().find(|(_,v)| **v == v1).unwrap();
                let (j, _) = p1.iter().enumerate().find(|(_,v)| **v == v2).unwrap();
                let (min, max) = min_max(i,j);
                let diff = max - min;
                for (i,_) in p1.iter().enumerate() {
                    graph.add_edge(p1[i], p1[(i+diff) % p1.len()]);
                    remaining_edge -= 1;
                }
            } else if p1.len() % p2.len() == 0 || p2.len() % p1.len() == 0 {
                let (mut min, mut max) = (p1,p2);
                if p2.len() < p1.len() {
                    min = p2;
                    max = p1;
                }
                let quotient = max.len() / min.len();
                for w in 0..min.len() {
                    let offset = 0;
                    for r in 0..quotient {
                        graph.add_edge(max[(r+offset)%max.len()], min[w]);
                        remaining_edge -= 1;
                    }
                }
            }
        }

        if !graph.is_connected() {
            // Take the smallest partition and make its vertices connect to each other.
            let smallest_partition = graph_partition.iter()
                .min_by(|x,y| x.len().cmp(&y.len())).unwrap();
            let ((i, _), (j, _)) = pick2(&smallest_partition.iter().enumerate().collect());
            let (min, max) = min_max(i, j);
            let diff = max - min;
            for (i,_) in smallest_partition.iter().enumerate() {
                graph.add_edge(smallest_partition[i], smallest_partition[(i+diff) % smallest_partition.len()]);
            }
        }
        graph
    }
}


#[cfg(test)]
mod tests {
    use graph::*;
    
    #[cfg(test)]
    fn dedup_refs(orbits: &Vec<Arc<Vec<u32>>>) -> Vec<Vec<u32>>{
        let mut result = vec![];
        'outer: for i in 0..orbits.len() {
            for j in 0..i {
                if Arc::ptr_eq(&orbits[i], &orbits[j]) {
                    continue 'outer;
                }
            }
            let cp = orbits[i].iter().cloned().collect();
            result.push(cp);
        }
        result
    }

    #[test]
    fn test_friendship_nodes_len_upto_100() {
        for i in 2..100 {
            let g = Graph::friendship_graph(i as u32);
            assert_eq!(i*2+1, g.nb_nodes());
        }
    }

    #[test]
    fn test_friendship_edges_len_upto_100() {
        for i in 2..100 {
            let g = Graph::friendship_graph(i as u32);
            assert_eq!(3*i, g.edges().len());
        }
    }

    #[test]
    fn test_friendship_girth_upto_50() {
        for i in 2..50 {
            let g = Graph::friendship_graph(i as u32);
            assert_eq!(3, g.girth());
        }
    }

    #[test]
    fn test_hoffman_singleton_nodes_len() {
        let g = Graph::hoffman_singleton();
        assert_eq!(50, g.nb_nodes());
    }
    
    #[test]
    fn test_hoffman_singleton_edges_len() {
        let g = Graph::hoffman_singleton();
        assert_eq!(175, g.nb_edges());
    }

    #[test]
    fn test_hoffman_singleton_girth() {
        let g = Graph::hoffman_singleton();
        assert_eq!(5, g.girth());
    }

    #[test]
    fn test_flower_snark_nodes_len_from_3_to_100() {
        for i in 3..100 {
            let g = Graph::flower_snark(i as u32);
            assert_eq!(4*i, g.nb_nodes());
        }
    }

    #[test]
    fn test_flower_snark_edges_len_from_3_to_100() {
        for i in 3..100 {
            let g = Graph::flower_snark(i as u32);
            assert_eq!(6*i, g.nb_edges());
        }
    }

    #[test]
    fn test_flower_snark_girth_from_3_to_20() {
        for i in 3..20 {
            let g = Graph::flower_snark(i as u32);
            println!("i: {}, girth: {}", i, g.girth());
            match i {
                3 => assert_eq!(3, g.girth()),
                5 => assert_eq!(5, g.girth()),
                n if n >= 7 => assert_eq!(6, g.girth()),
                _ => (),
            }
        }
    }

    #[test]
    fn test_hypercube_3_orbits() {
        let mut g = Graph::hyper_cube(3);
        g.calculate_aut_gr();
        g.pre_calculate_orbits();
        let expected = vec![c![i, for i in 0..2u32.pow(3)]];
        let actual = dedup_refs(&g.orbits);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_complete_graph_10() {
        let g = Graph::complete_graph(10);
        let expected = 45;
        assert_eq!(expected, g.nb_edges() as u32);
    }

    #[test]
    fn test_hypercube4_edges_len() {
        let n = 4;
        let g = Graph::hyper_cube(n);
        let expected = 2u32.pow(n-1) * n; 
        assert_eq!(expected, g.nb_edges() as u32);
    }

    #[test]
    fn test_hypercube4_nodes_len() {
        let n = 4;
        let g = Graph::hyper_cube(n);
        let expected = 2u32.pow(n);
        assert_eq!(expected, g.nb_nodes() as u32);
    }

    #[test]
    fn test_hypercube4_neighbors_len() {
        let n = 6;
        let g = Graph::hyper_cube(n);
        let expected = true;
        let actual = g.nodes.iter().all(|v| v.len() as u32 == n); 
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_random_graph_connectness_100_0dot8() {
        let g = Graph::random_graph(100, 0.8);
        assert!(g.is_connected());
    }

    #[test]
    fn test_random_graph_connectness_500_0dot6() {
        let g = Graph::random_graph(500, 0.6);
        assert!(g.is_connected());
    }

    #[test]
    fn test_random_graph_edges_100_0dot7() {
        let edges = Graph::edges_from_density(100, 0.7);
        let g = Graph::random_graph(100, 0.7);
        assert_eq!(edges, g.nb_edges() as u32);
    }

    #[test]
    fn test_random_graph_edges_100_0dot2() {
        let edges = Graph::edges_from_density(100, 0.2);
        let g = Graph::random_graph(100, 0.2);
        assert_eq!(edges, g.nb_edges() as u32);
    }

    #[test]
    fn test_edges_from_density_of_0dot8() {
        let expected = 36;
        let actual = Graph::edges_from_density(10,0.8);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_edges_from_density_of_1() {
        let expected = 45;
        let actual = Graph::edges_from_density(10, 1.0);
        assert_eq!(expected, actual);
    }


    #[test]
    fn test_edges_from_density_of_0dot2() {
        let expected = 9;
        let actual = Graph::edges_from_density(10,0.2);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_edges_from_density_of_0dot3() {
        let expected = 228228;
        let actual = Graph::edges_from_density(1234,0.3);
        assert_eq!(expected, actual);
    }
}

