use std::process::{Command, Stdio, Child};
use std::io::Write;

/// An interface to produce dot string and graph pictures.
pub trait DotFormat {
    /// Identifier for the current graph.
    fn dot_name(&self) -> String {
        "".to_owned()
    }

    /// Produce a PNG representing the current graph.
    fn into_png(&self, path: &String) -> std::io::Result<Child> {
        dot_to_png(&self.to_dot_string(), path)
    }
    
    /// Produce a string describing the current graph in the dot format.
    fn to_dot_string(&self) -> String;
}

/// Convert a dot string to a png.
pub fn dot_to_png(dot_string: &String, path: &String) -> std::io::Result<Child> {
    let mut dot_process = Command::new("dot")
        .arg("-Tpng")
        .arg("-o")
        .arg(path)
        .stdin(Stdio::piped())
        .spawn()?;
    dot_process.stdin.take().unwrap().write_all(dot_string.as_bytes())?;
    Ok(dot_process)
}