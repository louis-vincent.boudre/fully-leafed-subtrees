//! Range expression parser module
extern crate regex;
use self::regex::{Regex};


type Edges = Vec<(u32, u32)>;


pub fn parse_edges(expr: &String) -> Result<Edges, &'static str> {
    let edges_re = Regex::new(r"\((\d+)\s*,\s*(\d+)\)").unwrap();
    
    let mut edges = vec![];

    for cap in edges_re.captures_iter(expr) {
        let u: u32 = cap[1].parse().unwrap();
        let v: u32 = cap[2].parse().unwrap();
        edges.push((u,v));
    }

    Ok(edges)
    /*
    let start: usize;
    let end: usize;
    match caps.name("single") {
        Some(s) => {
            start = s.as_str().parse().unwrap();
            end   = start + 1;
            return Ok((Bound::Included(start), Bound::Excluded(end)));
        },
        _ => (),
    }
    if caps.name("start").is_some() && caps.name("end").is_some() {
        start = caps.name("start").unwrap().as_str().parse().unwrap();
        end   = caps.name("end").unwrap().as_str().parse().unwrap();
        return Ok((Bound::Included(start), Bound::Excluded(end)));
    }
    if caps.name("start").is_none() {
        end = caps.name("end").unwrap().as_str().parse().unwrap();
        return Ok((Bound::Unbounded, Bound::Excluded(end)));
    }
    start = caps.name("start").unwrap().as_str().parse().unwrap();
    return Ok((Bound::Included(start), Bound::Unbounded));
    */
}

