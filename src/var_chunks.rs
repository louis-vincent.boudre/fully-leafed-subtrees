//! Variable length chunks iterator.
//! 
//! For more information on chunks see [`std::slice::Chunk`]
//! 
//! [`std::slice::Chunk`]: https://doc.rust-lang.org/std/slice/struct.Chunks.html

/// An iterator over a slice in (non-overlapping) variable length chunks.
pub struct VariableChunks<'a, T: 'a> {
    v: &'a [T],
    chunk_sizes: Vec<usize>,
}

impl <'a, T> VariableChunks<'a, T> {
    pub fn new(v: &'a [T], chunk_sizes: Vec<usize>) -> VariableChunks<T> {
        VariableChunks{ v, chunk_sizes }
    }
}

impl<'a, T> Clone for VariableChunks<'a, T> {
    fn clone(&self) -> VariableChunks<'a, T> {
        VariableChunks {
            v: self.v,
            chunk_sizes: self.chunk_sizes.to_vec(),
        }
    }
}

impl<'a, T> Iterator for VariableChunks<'a, T> {
    type Item = &'a [T];

    #[inline]
    fn next(&mut self) -> Option<&'a [T]> {
        use std::cmp::{min};
        if self.v.is_empty() {
            None
        } else {
            let chunksz = min(self.v.len(), self.chunk_sizes[0]);
            self.chunk_sizes.remove(0);
            let (fst, snd) = self.v.split_at(chunksz);
            self.v = snd;
            Some(fst)
        }
    }
}

#[cfg(test)]
mod tests {
    use var_chunks::{VariableChunks};

    #[test]
    fn test_variable_chunks_1() {
        let data: Vec<usize> = (0..10).collect();
        let sizes = vec![2,3,1,1,3];
        let mut iter = VariableChunks::new(&data, sizes);
        assert_eq!(iter.next().unwrap(), &[0,1]);
        assert_eq!(iter.next().unwrap(), &[2,3,4]);
        assert_eq!(iter.next().unwrap(), &[5]);
        assert_eq!(iter.next().unwrap(), &[6]);
        assert_eq!(iter.next().unwrap(), &[7,8,9]);
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_variable_chunks_2() {
        let data: Vec<usize> = (0..10).collect();
        let sizes = vec![1;10];
        let mut iter = VariableChunks::new(&data, sizes);
        assert_eq!(iter.next().unwrap(), &[0]);
        assert_eq!(iter.next().unwrap(), &[1]);
        assert_eq!(iter.next().unwrap(), &[2]);
        assert_eq!(iter.next().unwrap(), &[3]);
        assert_eq!(iter.next().unwrap(), &[4]);
        assert_eq!(iter.next().unwrap(), &[5]);
        assert_eq!(iter.next().unwrap(), &[6]);
        assert_eq!(iter.next().unwrap(), &[7]);
        assert_eq!(iter.next().unwrap(), &[8]);
        assert_eq!(iter.next().unwrap(), &[9]);
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_variable_chunks_3() {
        let data: Vec<usize> = (0..10).collect();
        let sizes = vec![10];
        let mut iter = VariableChunks::new(&data, sizes);
        assert_eq!(iter.next().unwrap(), &[0,1,2,3,4,5,6,7,8,9]);
        assert!(iter.next().is_none());
    }
}