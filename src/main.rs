#![feature(test, slice_concat_ext, integer_atomics)]

use std::process::Command;
use std::env;
use std::fs::{File, OpenOptions};
use std::io::{Write};

mod edges_expr_parser;
mod graph;
mod graphviz;
mod configuration;
mod solver;
mod sagemath;
mod simple_logger;
mod goal_solver;
mod atomic_vec;
mod range_expr_parser;
mod var_chunks;
mod number;
mod utility;

extern crate rand;

extern crate getopts;
use getopts::Options;

#[macro_use(c)]
extern crate cute;

#[macro_use]
extern crate log;

use log::{LevelFilter};

use solver::{OptionsBuilder, Solver};
use range_expr_parser::{to_range, MixedRange, no_range};
use edges_expr_parser::{parse_edges};

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} 
        [--help] [--verbose] 
        [--graph graph-type]
        [--edges \"(u,v),(x,y), ...\"
        [--num-vertices number] 
        [--num-vertices2 number]
        [--density [0.2; 0.8]] 
        [--dimension number] 
        [--num-contexts number] 
        [--num-threads number] 
        [--strategy (naive|dist)]
        [--list-graph]", program);
        
    print!("{}", opts.usage(&brief));
}

fn clear_tmp() -> std::io::Result<()> {
    let mut child = Command::new("mkdir")
        .arg("-p")
        .arg("tmp")
        .spawn()?;
    match child.wait() {
        Err(err) => panic!("Faoled to create tmp folder: {:?}", err.kind()),
        _ => ()
    }
    let mut child = Command::new("rm")
        .arg("-fr")
        .arg("tmp/*")
        .spawn()?;
    match child.wait() {
        Err(err) => panic!("Failed to clean tmp folder: {:?}", err.kind()),
        _ => ()
    }
    Ok(())
}

fn main() {
    match clear_tmp() {
        Err(err) => panic!("Failed to prepare tmp folder: {:?}", err.kind()),
        _ => ()
    }

    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optopt("t", "num-threads",     "Sets the number of maximum thread.
                                         The default value is 0 (singlethreaded)", 
                                        "number");
                                        
    opts.optopt("g", "graph",           "Sets the graph to use.
                                         The default value is the complete graph.", 
                                        "[cycle|wheel|complete|bipartite|random|hypercube|snark|hoffman-singleton]");
                                        
    opts.optopt("v", "num-vertices",    "Sets the number of vertices.
                                         The default value is 10.", 
                                        "number");

    opts.optopt("u", "num-vertices2",   "Second number of vertices, only works for the bipartite graph, otherwise it is ignored.
                                         The default value is 10.", 
                                        "number");

    opts.optopt("d", "density",         "Sets the density of a graph. This argument works only on random graph, otherwise ignored.
                                         The default value is 0.8.", 
                                        "[0.2; 0.8]");
                                        
    opts.optopt("D", "dimension",       "Sets the dimension of an hypercube. This argument works only on hypecube graph, otherwise ignored.
                                         The default value is 3", 
                                        "number");
    
    opts.optopt("e", "edges",           "Build a graph from a set of edges : (u,v), (x,y), ...
                                         Vertices must range from 0 to n-1",
                                         "");
    
    opts.optopt("G", "graphviz",        "Generate `tree.png` to a certain depth level provided in argument.
                                         This feature doesn't work in multithreading neither in multi context.",
                                        "range (a..b|a..|..b|a|..)");

    opts.optopt("S", "symmetries",      "Avoid symmetries while calculating leaf map.", 
                                        "range (a..b|a..|..b|a|..)");
    
    opts.optopt("b", "bound",           "Use a bound strategy to calculate the leaf potential.
                                         When the bound is set to 0 it is ignored,
                                         when it's set to -1 is the only bound strategy used,
                                         when it's set to any number > 0, it will be the threshold until
                                         the configuration goes back to the naive leaf potential function.
                                         Example: ./fully-leafed -g custom -v 20 -b 10, will use the bound strategy until its number of
                                         excluded vertices + the size of its subtree is equal 10, after that it fallback to the naive approche.
                                        ", 
                                        "range (a..b|a..|..b|a|..)");

    opts.optopt("f", "fixed",           "Fixed the subtree size",
                                        "number");
    opts.optflag("l", "list-graph",     "Lists the entire graph that can be pass to --graph option");
    opts.optflag("B", "benchmark",       "Execute a benchmark on multiple graph.
                                         Ignores everyother arguments.");
    opts.optflag("h", "help", "print this help menu");
    opts.optflag("V", "verbose", "prints information messages in the console during the execution");
    
    if args.len() == 1 {
        print_usage(&program, opts);
        return;
    }

    
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!(f.to_string()) }
    };

    if matches.opt_present("l") {
        let graph_str = [
            "cycle(num-vertices)",
            "wheel(num-vertices)",
            "compelte(num-vertices)",
            "bipartite(num-vertices, num-vertices2)",
            "random(num-vertices, density)",
            "hypercube(dimension)",
            "snark(num-vertices)",
            "hoffman-singleton()",
            "friendship(num-vertices)",
            "generalized-peterson(num-vertices, num-vertices2)",
            "random-sym(num-vertices, density)"
        ];
        println!("{}", graph_str.join("\n"));
    }

    if matches.opt_present("h") {
        print_usage(&program, opts);
        return;
    }
    
    if matches.opt_present("B") {
        benchmark().expect("Failed to do the benchmark");
        return;
    }

    if matches.opt_present("g") || matches.opt_present("e") {
        
        if matches.opt_str("g").is_none() && matches.opt_str("e").is_none() {
            print_usage(&program, opts);
            return;
        }

        let mut options_builder = OptionsBuilder::new();
        let mut num_vertices: u32 = 10;
        let mut num_vertices2: u32 = 10;
        let mut density: f32 = 0.8;
        let mut dimension: u32 = 0;
        let mut num_threads: usize = 0;
        let mut bound: MixedRange = no_range();
        let mut symmetries: MixedRange = no_range();
        let mut graphviz: MixedRange = no_range();
        let mut fixed: Option<usize> = None;

        // fixed
        match matches.opt_str("f") {
            Some(s) => fixed = Some(s.parse().unwrap()),
            _ => ()
        }

        // bound
        match matches.opt_str("b") {
            Some(s) => { 
                match to_range(&s) {
                    Ok(range) => bound = range,
                    Err(why) => panic!(why),
                }
            },
            _ => ()
        }

        // num-vertices
        match matches.opt_str("v") {
            Some(s) => num_vertices = s.parse().unwrap(),
            _ => ()
        }

        match matches.opt_str("u") {
            Some(s) => num_vertices2 = s.parse().unwrap(),
            _ => ()
        }
                
        // density
        match matches.opt_str("d") {
            Some(s) => density = s.parse().unwrap(),
            _ => ()
        }
        // num-threads
        match matches.opt_str("t") {
            Some(s) => num_threads = s.parse().unwrap(),
            _ => ()
        }

        // symmetry level
        match matches.opt_str("S") {
            Some(s) => { 
                match to_range(&s) {
                    Ok(range) => symmetries = range,
                    Err(why) => panic!(why),
                }
            },
            _ => ()
        }
        
        match matches.opt_str("D") {
            Some(s) => dimension = s.parse().unwrap(),
            _ => ()
        }

        // graphviz level
        match matches.opt_str("G") {
            Some(s) => { 
                match to_range(&s) {
                    Ok(range) => graphviz = range,
                    Err(why) => panic!(why),
                }
            },
            _ => ()
        }
        
        let mut graph: graph::Graph;
        let mut graph_type = String::from("from edges");
        if let Some(x) = matches.opt_str("g") {
            graph_type = x; 
            //graph: Option<graph::Graph> = None;
            match graph_type.as_str() {
                "cycle"                 => graph = graph::Graph::cycle_graph(num_vertices),
                "complete"              => graph = graph::Graph::complete_graph(num_vertices),
                "bipartite"             => graph = graph::Graph::complete_bipartite_graph(num_vertices, num_vertices2),
                "wheel"                 => graph = graph::Graph::wheel_graph(num_vertices),
                "random"                => graph = graph::Graph::random_graph(num_vertices, density),
                "hypercube"             => graph = graph::Graph::hyper_cube(dimension),
                "snark"                 => graph = graph::Graph::flower_snark(num_vertices),
                "hoffman-singleton"     => graph = graph::Graph::hoffman_singleton(),
                "friendship"            => graph = graph::Graph::friendship_graph(num_vertices),
                "generalized-peterson"  => graph = graph::Graph::generalized_peterson(num_vertices, num_vertices2),
                "random-sym"            => graph = graph::Graph::random_sym_graph(num_vertices, density),
                unknown => {
                    println!("Unknown graph type: `{}`", unknown);
                    print_usage(&program, opts);
                    return;
                }
            }
        } else if let Some(edges_str) = matches.opt_str("e") {
            match parse_edges(&edges_str) {
                Ok(edges) => graph = graph::Graph::from(edges),
                Err(why) => panic!(why),
            } 
        } else {
            print_usage(&program, opts);
            return; 
        }

        if matches.opt_present("V") {
            match simple_logger::init(LevelFilter::Info) {
                Err(_) => panic!("Failed to initialize logger"),
                Ok(_) => ()
            }
        }
        
        match fixed {
            None => (),
            Some(index) => options_builder = options_builder.fixed_at(index),
        }
        
        let opts = options_builder
            .num_threads(num_threads)
            .graphviz(graphviz)
            .bound(bound)
            .symmetries(symmetries)
            .finish();

        let (result, time) = Solver::leaf_function(graph, opts);
        println!("Result for {} graph : {:?}, in {}ms", graph_type, result, time);
    }
}


fn benchmark() -> std::io::Result<()> {
    use graph::{Graph};
    use std::ops::{Bound};
    let mut dense_times = vec![];
    let mut undense_times = vec![];
    let mut snark_times = vec![];
    let mut friendship_times = vec![];
    
    let dense_range = 10..100;
    let undense_range = 10..50;
    let snark_range = 3..7;
    let friendship_range = 10..15;

    dense_times.push(dense_range.clone().step_by(10).collect());  // time columns
    undense_times.push(undense_range.clone().step_by(10).collect());
    snark_times.push(snark_range.clone().collect());
    friendship_times.push(friendship_range.clone().collect());
    
    let mut fs: Vec<Option<File>> = (0..4).map(|t| OpenOptions::new().write(true)
                    .truncate(true)
                    .create(true)
                    .open(format!("bench_results/result_{}.dat", t))
                    .unwrap()).map(|f| Some(f)).collect();
    
    let (f1,f2,f3,f4) = (fs[0].take().unwrap(), 
                         fs[1].take().unwrap(), 
                         fs[2].take().unwrap(), 
                         fs[3].take().unwrap());
    let threads = [1,2];

    for t in threads.iter() {
        let mut time_vec = vec![];
        for i in dense_range.clone().step_by(10) {
            let g = Graph::random_graph(i as u32, 0.8);
            let options = OptionsBuilder::new()
                .num_threads(*t)
                .finish();
            let (_, time) = Solver::leaf_function(g, options);
            time_vec.push(time);
        }
        dense_times.push(time_vec.to_vec());

        time_vec.clear();
        for i in undense_range.clone().step_by(10) {
            let g = Graph::random_graph(i as u32, 0.2);
            let options = OptionsBuilder::new()
                .num_threads(*t)
                .finish();
            let (_, time) = Solver::leaf_function(g, options);
            time_vec.push(time);
        }
        undense_times.push(time_vec.to_vec());

        time_vec.clear();
        for i in snark_range.clone() {
            let g = Graph::flower_snark(i as u32);
            let options = OptionsBuilder::new()
                .num_threads(*t)
                .symmetries((Bound::Unbounded, Bound::Excluded(10)))
                .finish();
            let (_, time) = Solver::leaf_function(g.clone(), options);
            time_vec.push(time);
        }
        snark_times.push(time_vec.to_vec());

        time_vec.clear();
        for i in friendship_range.clone() {
            let g = Graph::friendship_graph(i as u32);
            let options = OptionsBuilder::new()
                .num_threads(*t)
                .symmetries((Bound::Unbounded, Bound::Excluded(5)))
                .finish();
            let (_, time) = Solver::leaf_function(g.clone(), options);
            time_vec.push(time);
        }
        friendship_times.push(time_vec.to_vec());
    }
    
    let mut fs = [(f1, dense_times),(f2, undense_times),(f3, snark_times),(f4, friendship_times)];
    fs.iter_mut().for_each(|(f, data)| {
        println!("data: {:?}", data);
        write_bench_results(f, data, threads.len()).expect("Failed to write bench result");
    });
    Ok(())
}

fn write_bench_results(f: &mut File, data: &Vec<Vec<u64>>, num_threads: usize) -> std::io::Result<()> {
    let column_str = (0..=num_threads).map(|t| { 
        if t == 0 {
            "time".to_owned()
        } else {
            let suffix;
            if t == 1 {
                suffix = "thread"
            } else {
                suffix = "threads"
            }
            format!("\"{} {}\"", t, suffix)
        }
    }).collect::<Vec<String>>().join("\t");
    writeln!(f, "{}", column_str)?;
    for i in 0..data[0].len() {
        let mut entry_str = vec![];
        for j in 0..data.len() {
            println!("j: {}, i: {}", j, i);
            entry_str.push(data[j][i].to_string());
        }
        writeln!(f, "{}", entry_str.join("\t"))?;
    }
    Ok(())
}
