extern crate test;

use std::sync::{Arc, Mutex};
use std::process::{Child};
use std::time::{Instant};
use std::sync::atomic::{AtomicI32, Ordering};
use std::ops::{RangeBounds};

use configuration::*;
use graph::*;
use goal_solver::{GoalSolver, WorkDispatcher};
use graphviz::{DotFormat, dot_to_png};
use atomic_vec::{AtomicVecI32};
use range_expr_parser::{MixedRange, no_range};


/// Options type represents the configurable options that can be send to
/// the leaf map solver.
pub struct Options {
    /// Number of threads usable by the solver.
    num_threads: usize,
    /// A range that limits the section of the decision-tree to collect dot strings.
    graphviz: MixedRange,
    /// A range that limits the section of the decision-tree to ignore graph's automorphism.
    symmetries: MixedRange,
    /// A range that limits the section of the decision-tree to use the bound strategy.
    bound: MixedRange,
    /// The fixed size of the tree to calculate the maximum number of leaves.
    fixed_at: Option<usize>,
}

/// Builds an options using the Builder design pattern.
pub struct OptionsBuilder {
    num_threads: Option<usize>,
    graphviz: Option<MixedRange>,
    symmetries: Option<MixedRange>,
    bound: Option<MixedRange>,
    fixed_at: Option<usize>,
}

impl OptionsBuilder {
    pub fn new() -> OptionsBuilder {
        OptionsBuilder {
            num_threads: None,
            graphviz: None,
            symmetries: None,
            bound: None,
            fixed_at: None,
        }
    }

    pub fn num_threads(mut self, num_threads: usize) -> OptionsBuilder {
        self.num_threads = Some(num_threads);
        self
    }
    
    pub fn graphviz(mut self, graphviz: MixedRange) -> OptionsBuilder {
        self.graphviz = Some(graphviz);
        self
    }

    pub fn symmetries(mut self, symmetries: MixedRange) -> OptionsBuilder {
        self.symmetries = Some(symmetries);
        self
    }

    pub fn bound(mut self, bound: MixedRange) -> OptionsBuilder {
        self.bound = Some(bound);
        self
    }

    pub fn fixed_at(mut self, index: usize) -> OptionsBuilder {
        self.fixed_at = Some(index);
        self
    }

    pub fn finish(&self) -> Options {
        Options {
            num_threads: match self.num_threads {
                Some(val) => val,
                None => 0
            },
            graphviz: match self.graphviz {
                Some(val) => val,
                None => no_range(),
            },
            symmetries: match self.symmetries {
                Some(val) => val,
                None => no_range(),
            },
            bound: match self.bound {
                Some(val) => val,
                None => no_range(),
            },
            fixed_at: self.fixed_at,
        }
    }
}

#[derive(Debug)]
/// The Either type represents values with two possibilities.
/// Either<T1,T2> is either choice A or choice B.
pub enum Either<T1,T2> {
    /// Choice A
    A(T1),
    /// Choice B
    B(T2),
}

/// Vector containing tuple that represents the following association: dot name => dot string
type DotCollector = Vec<(String, String)>;

pub struct Solver;

impl Solver {

    /// Calcule the leaf map of a graph.
    /// It calculates either the entire map (all subtree size) or
    /// the maximum number of leaf at a fixed size.
    /// 
    /// # Examples
    /// 
    /// ```
    /// use solver::*;
    /// use graph::*;
    /// 
    /// let g = Graph::hyper_cube(3);
    /// let options = OptionsBuilder::new().finish();
    /// let (result, _time) = Solver::leaf_function(g, options);
    /// if let Either::A(actual) = result {
    ///     assert_eq!(vec![0, 0, 2, 2, 3, 2, -2147483648, -2147483648, -2147483648], actual);
    /// }
    /// ```
    /// 
    /// ```
    /// use solver::*;
    /// use graph::*;
    /// 
    /// let g = Graph::hyper_cube(3);
    /// let options = OptionsBuilder::new()
    ///     .fixed_at(4)
    ///     .finish();
    /// let (result, _time) = Solver::leaf_function(g, options);
    /// if let Either::B(actual) = result {
    ///     assert_eq!(3, actual);
    /// }
    /// ```
    pub fn leaf_function(mut graph: Graph, opts: Options) -> (Either<Vec<i32>, i32>, u64) {
        let mixed_strategies = MixedStrategies {
            bound_range: opts.bound,
            symmetry_range: opts.symmetries,
        };
        if opts.symmetries != no_range() {
            graph.calculate_aut_gr();
        }
        let graph = Arc::new(graph);

        let configuration = UInt32ConfigurationSym::new(Arc::clone(&graph), mixed_strategies);
        let now = Instant::now();
        let result;
        match opts.fixed_at {
            None => result = Either::A(Solver::inner_leaf_function(configuration, opts.num_threads, opts.graphviz)),
            Some(index) => result = Either::B(Solver::fixed_leaf_function(configuration, index, opts.num_threads)),
        }
        let duration = now.elapsed();
        let ms = (duration.as_secs() * 1_000) + (duration.subsec_nanos() / 1_000_000) as u64;
        (result, ms)
    }

    /// Backtrack a configuration by verifying the checkpoints marked by earlier dispatches.
    /// 
    fn backtrack<T>(configuration: &mut T, checkpoints: &mut Vec<usize>, steps: &mut usize) -> bool
        where T: Configuration<N=u32>,
    {
        loop {
            let (has_more, steps_back) = configuration.backtrack();
            if !has_more { 
                return false;
            }
            *steps -= steps_back as usize;
            let top = checkpoints.last().cloned();
            match top {
                Some(last) => {
                    if last == *steps {
                        checkpoints.pop();
                        continue;
                    } else {
                        return true;
                    }
                },
                None => return true,
            }
        }
    }
    
    fn inner_leaf_function<Unit>(configuration: Unit, num_threads: usize, graphviz_level: MixedRange) -> Vec<i32> 
        where Unit: Configuration<N=u32> + Clone + Send + 'static
    {
        type Goal = AtomicVecI32;
        let list = AtomicVecI32::from(vec![i32::min_value(); configuration.nb_nodes()+1]);
        let solver = GoalSolver::new(num_threads, list, configuration);
        let dot_collector: Arc<Mutex<DotCollector>>= Arc::new(Mutex::new(vec![]));
        let listener_dot_collector = dot_collector.clone();

        let final_goal = solver.solve(move |goal: Arc<Goal>, mut work, wd: WorkDispatcher<Unit>| {
            let mut excluding = false;
            let mut steps: usize = 0;
            let mut checkpoints: Vec<usize> = vec![];
            'outer: loop {
                if graphviz_level.contains(&work.original_history_len()) {
                    let mut collector = listener_dot_collector
                        .lock()
                        .expect("Error: failed to acquire the dot string collector on pre-dispatch.");
                    collector.push((work.dot_name(), work.to_dot_string()));
                }
                let u0 = work.vertex_to_add();
                if u0.is_none() {
                    let i = work.subtree_size();
                    let l = work.num_leaves() as i32;
                    let mut old = goal.load(i);
                    loop {
                        if l <= old {
                            break;
                        }
                        match goal.compare_exchange_weak(i, old, l) {
                            Ok(_) => break,
                            Err(x) => old = x,
                        }
                    }
                    match Solver::backtrack(&mut work, &mut checkpoints, &mut steps) {
                        false => break,
                        _ => excluding = true,
                    }
                } else {
                    let u = u0.unwrap();
                    let mut promising = false;
                    let n = work.subtree_size();
                    let m = work.nb_nodes() + 1 - work.num_excluded();
                    
                    for i in n..m {
                        if n <= 2 {
                            promising = true;
                            break; 
                        }
                        if goal.load(i) < work.leaf_potential(i) as i32 {
                            promising = true;
                            break;
                        }
                    }
                    
                    if !promising {
                        match Solver::backtrack(&mut work, &mut checkpoints, &mut steps) {
                            false => break,
                            _ => excluding = true,
                        }
                        continue;
                    }
                    if !excluding && 
                       work.num_border() > 1
                    {
                        if let Some(transaction) = wd.try_acquire() {
                            let rnd = rand::random::<bool>();
                            let mut disposable_work = work.clone();
                            match rnd {
                                true => { disposable_work.include_vertex(u); },
                                false => { disposable_work.exclude_vertex(u); },
                            }
                            disposable_work.make_rooted();
                            let next = disposable_work.vertex_to_add();
                            if next.is_some() && transaction.send(disposable_work) {
                                match rnd {
                                    true => excluding = true,
                                    false => checkpoints.push(steps),
                                }
                            }
                        }
                    }
                    if excluding {
                        excluding = false;
                        work.exclude_vertex(u);
                    } else {
                        work.include_vertex(u);
                    }
                    steps += 1;
                }
            }
        });

        if graphviz_level != no_range() {
            let tree: DotCollector;
            let mut collector = dot_collector;
            loop {
                match Arc::try_unwrap(collector) {
                    Ok(mutex) => { 
                        tree = mutex.into_inner().unwrap();
                        break;
                    },
                    Err(err) => collector = err,
                }
            }
            tree.into_png(&"tree.png".to_owned()).unwrap().wait().unwrap();
        }
        return final_goal.into_inner();//.unwrap();
    }

    fn fixed_leaf_function<Unit: Configuration<N=u32> + Clone + Send + 'static>(configuration: Unit, index: usize, num_threads: usize) -> i32 {
        let initial_value = AtomicI32::new(<i32>::min_value());
        let solver = GoalSolver::new(num_threads, initial_value, configuration);
        let result = solver.solve(move |goal: Arc<AtomicI32>, mut work, wd: WorkDispatcher<Unit>| {
            let mut excluding = false;
            let mut steps: usize = 0;
            let mut checkpoints: Vec<usize> = vec![];
            'outer: loop {
                let u0 = work.vertex_to_add();
                let size = work.subtree_size();
                if u0.is_none() || size == index {
                    if size == index {
                        let l = work.num_leaves() as i32;
                        let mut old = goal.load(Ordering::Acquire);
                        loop {
                            if l <= old {
                                break;
                            }
                            match goal.compare_exchange_weak(old, l, Ordering::AcqRel, Ordering::Relaxed) {
                                Ok(_) => break,
                                Err(x) => old = x,
                            }
                        }
                    }
                    match Solver::backtrack(&mut work, &mut checkpoints, &mut steps) {
                        false => break,
                        _ => excluding = true,
                    }
                } else {
                    let u = u0.unwrap();
                    let mut promising = false;
                    if goal.load(Ordering::Acquire) < work.leaf_potential(index) as i32 {
                        promising = true;
                    }
                    if !promising {
                        match Solver::backtrack(&mut work, &mut checkpoints, &mut steps) {
                            false => break,
                            _ => excluding = true,
                        }
                        continue;
                    }

                    if !excluding && 
                       work.num_border() > 1
                    {
                        if let Some(transaction) = wd.try_acquire() {
                            let rnd = rand::random::<bool>();
                            let mut disposable_work = work.clone();
                            match rnd {
                                true => { disposable_work.include_vertex(u); },
                                false => { disposable_work.exclude_vertex(u); },
                            }
                            disposable_work.make_rooted();
                            if transaction.send(disposable_work) {
                                match rnd {
                                    true => excluding = true,
                                    false => checkpoints.push(steps),
                                }
                            }
                        }
                    }
                    if excluding {
                        excluding = false;
                        work.exclude_vertex(u);
                    } else {
                        work.include_vertex(u);
                    }
                    steps += 1;
                }
            }
        }); 
        result.into_inner()
    }

}

impl DotFormat for DotCollector {
    fn to_dot_string(&self) -> String {
        let header_str = "digraph { \n\tnode [shape=box];".to_owned();
        let names: Vec<String> = self.iter()
            .map(|(name, _)| name.clone())
            .collect();
        
        let body_str = names.iter()
            .map(|name| format!("\t\"{}\" [image=\"tmp/{}.png\", label=\"\"];", name, name))
            .collect::<Vec<String>>()
            .join("\n");
        
        let pop = |s: &String| -> String { 
            s.chars().take(s.len() - 1).collect()
        };
        
        let edges = names.iter()
            .skip(1)
            .map(|name| format!("\t\"{}\" -> \"{}\";", pop(&name), name))
            .collect::<Vec<String>>()
            .join("\n");

        let footer = "}".to_owned();
        return vec![header_str, body_str, edges, footer].join("\n");
    }

    fn into_png(&self, path: &String) -> std::io::Result<Child> {
        let mut child_procs = vec![];
        for (name, dot_string) in self.iter() {
            let path = "tmp/".to_owned() + &name + &".png".to_owned();
            let temp = dot_to_png(dot_string, &path);
            child_procs.push(temp);
        }
        
        child_procs.drain(..)
            .for_each(|cp| { 
                cp.unwrap().wait().unwrap(); 
            });

        dot_to_png(&self.to_dot_string(), path)
    }
}

#[cfg(test)]
mod tests {
    use solver::*;

    fn buildOptions() -> Options {
        Options {
            num_threads: 0,
            graphviz: no_range(),
            symmetries: no_range(),
            bound: no_range(),
            fixed_at: None,
        }
    }

    fn buildOptionsThreaded(t: usize) -> Options {
        Options {
            num_threads: t,
            graphviz: no_range(),
            symmetries: no_range(),
            bound: no_range(),
            fixed_at: None,
        }
    }

    fn build_mixed_strategy() -> MixedStrategies {
        MixedStrategies {
            bound_range: no_range(),
            symmetry_range: no_range()
        }
    }

    #[test]
    fn test_leaf_map_with_complete_graph7() {
        let g = Graph::complete_graph(7);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,none,none,none,none,none];
        if let Either::A(actual) = Solver::leaf_function(g, buildOptions()).0 {
            assert_eq!(expected, actual);
        }
    }

    #[test]
    fn test_leaf_map_with_cycle_graph10() {
        let g = Graph::cycle_graph(10);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,2,2,2,2,2,2,none];
        if let Either::A(actual) = Solver::leaf_function(g, buildOptions()).0 {
            assert_eq!(expected, actual);
        }
    }

    #[test]
    fn test_leaf_map_with_complete_bipartite_graph_7_5() {
        let g = Graph::complete_bipartite_graph(7,5);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,5,6,7,none,none,none,none];
        if let Either::A(actual) = Solver::leaf_function(g, buildOptions()).0 {
            assert_eq!(expected, actual);
        }
    }

    #[test]
    fn test_leaf_map_with_cube_graph3() {
        let g = Graph::hyper_cube(3);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,2,none,none,none];
        if let Either::A(actual) = Solver::leaf_function(g, buildOptions()).0 {
            assert_eq!(expected, actual);
        }
    }

    #[test]
    fn test_leaf_map_with_cube_graph4() {
        let g = Graph::hyper_cube(4);
        let none = -2147483648;
        let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
        if let Either::A(actual) = Solver::leaf_function(g, buildOptions()).0 {
            assert_eq!(expected, actual);
        }
    }


    #[test]
    fn test_leaf_map_of_complete_graph_upto_50() {
        let none = -2147483648;
        for t in 1..5 {
            for i in 10..51 {
                let g = Graph::complete_graph(i);
                
                //let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
                let expected: Vec<i32> = (0..i+1).map(|x| {
                    match x {
                        0 | 1 => 0,
                        2 => 2,
                        _ => none
                    }
                }).collect();
                if let Either::A(actual) = Solver::leaf_function(g, buildOptionsThreaded(t)).0 {
                assert_eq!(expected, actual);
                }
            }
        }
        
    }

    #[test]
    fn test_leaf_map_of_cycle_graph_upto_50() {
        let mut n: i32 = 10;
        let none = -2147483648;
        for t in 1..5 {
            while n <= 50 {
                let g = Graph::cycle_graph(n as u32);
                
                //let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
                let expected: Vec<i32> = (0..n+1).map(|i| {
                    if i == 0 || i == 1 {
                        return 0;
                    }
                    if 2 <= i && i < n {
                        return  2;
                    }
                    return none;
                }).collect();
                if let Either::A(actual) = Solver::leaf_function(g, buildOptionsThreaded(t)).0 {
                    assert_eq!(expected, actual, "n: {}", n);
                }
                n += 1;
            }
        }
        
    }

    #[test]
    fn test_leaf_map_of_wheel_graph_upto_25() {
        let mut n: i32 = 4;
        let none = -2147483648;
        for t in 1..5 {
            while n <= 25 {
                let g = Graph::wheel_graph(n as u32);
                
                //let expected: Vec<i32> = vec![0,0,2,2,3,4,3,4,3,4,none,none,none,none,none,none,none];
                let expected: Vec<i32> = (0..n+2).map(|i| {
                    if i == 0 || i == 1 {
                        return 0;
                    }
                    if i == 2 {
                        return 2;
                    }
                    if i >= 3 && i <= (n/2 + 1) {
                        return i - 1;
                    }
                    if (n/2+2) <= i && i <= n - 1 {
                        return 2;
                    }
                    return none;
                }).collect();
                if let Either::A(actual) = Solver::leaf_function(g, buildOptionsThreaded(t)).0 {
                    assert_eq!(expected, actual, "n: {}", n);
                }
                n += 1;
            }
        }
        
    }
}