use std::sync::atomic::{AtomicI32, Ordering};

pub struct AtomicVecI32 {
    vector: Vec<AtomicI32>,
}

impl AtomicVecI32 {
    pub fn from(mut source: Vec<i32>) -> AtomicVecI32 {
        let vector = source.drain(..).map(|i| AtomicI32::new(i)).collect();

        AtomicVecI32 {
            vector
        }
    }
    #[allow(dead_code)]
    pub fn load(&self, i: usize) -> i32 {
        self.vector[i].load(Ordering::Acquire)
    }

    #[allow(dead_code)]
    pub fn store(&self, i: usize, value: i32) {
        self.vector[i].store(value, Ordering::Release)
    }

    #[allow(dead_code)]
    pub fn compare_and_swap(&mut self, i: usize, current: i32, new: i32) -> i32 {
        self.vector[i].compare_and_swap(current, new, Ordering::AcqRel)
    }

    #[allow(dead_code)]
    pub fn compare_exchange(&mut self, i: usize, current: i32, new: i32) -> Result<i32, i32> {
        self.vector[i].compare_exchange(current, new, Ordering::AcqRel, Ordering::Relaxed)
    }

    #[allow(dead_code)]
    pub fn compare_exchange_weak(&self, i: usize, current: i32, new: i32) -> Result<i32, i32> {
        self.vector[i].compare_exchange_weak(current, new, Ordering::AcqRel, Ordering::Relaxed)
    }

    #[allow(dead_code)]
    pub fn len(&self) -> usize {
        self.vector.len()
    }

    #[allow(dead_code)]
    pub fn into_inner(mut self) -> Vec<i32> {
        self.vector.drain(..).map(|a| a.load(Ordering::Relaxed)).collect()
    }
}
