use std::collections::{HashMap, HashSet, BinaryHeap, BTreeSet,VecDeque};
use std::cmp::{min};
use std::sync::{Arc};
use std::ops::{RangeBounds};

use graph::*;
use graphviz::{DotFormat};
use range_expr_parser::{MixedRange};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Status {
    Included,
    Excluded,
    Border,
    NotSeen
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum StatusInfo<T> {
    Degree(u32),
    Vertex(T),
    Nil
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct MixedStrategies {
    pub bound_range: MixedRange,
    pub symmetry_range: MixedRange,
}

use self::Status::*;
use self::StatusInfo::*;

#[derive(Debug,Clone)]
pub struct UInt32Configuration {
    vertex_status: Vec<(Status, StatusInfo<u32>)>,
    subtree_vertices: Vec<u32>,
    subtree_size: usize,
    num_leaves: usize,
    num_excluded: usize,
    border_size: usize,
    history: Vec<(u32, Status)>,
    original_history: Vec<Status>,
    mixed_strategy: MixedStrategies,
    graph: Arc<Graph>,
}

#[derive(Debug, Clone)]
pub struct UInt32ConfigurationSym {
    configuration: UInt32Configuration,
    forbidden_patterns: Vec<Pattern>,
    excluded_history: Vec<Vec<(u32, (Status, StatusInfo<u32>))>>,
    symmetrie_level: usize
}

pub trait Configuration: Clone + DotFormat {
    type N;

    fn new(g: Arc<Graph>, ms: MixedStrategies) -> Self where Self: Sized;

    /// Includes a vertex to the current configuration
    /// 
    /// # Arguments
    /// 
    /// * `v`- A valid vertex whose status is either NotSeen or Border
    /// 
    fn include_vertex(&mut self, v: Self::N) -> u32;
    
    /// Excludes a vertex from the current configuration
    /// 
    /// # Arguments
    /// 
    /// * `v` - A valid vertex whose status is Border or NotSeen if the subtree's size is 0.
    ///
    fn exclude_vertex(&mut self, v: Self::N);

    /// Undo the last operation done on the current configuration
    fn undo(&mut self) -> (Self::N, Status);

    /// Call undo as long there's history and as long as the 
    /// top of the history stack is not an Included vertex.
    /// 
    /// Returns a tuple : (history_not_empty, number_of_backtrack_steps)
    fn backtrack(&mut self) -> (bool, u32);

    /// Determines the leaf potential of the current configuration for a given size
    /// 
    /// # Arguments
    /// 
    /// * `i` - The size to test against
    ///
    fn leaf_potential(&self, i: usize) -> u32;
    
    fn num_leaves(&self) -> usize;

    fn num_excluded(&self) -> usize;

    fn num_border(&self) -> usize;

    fn subtree_size(&self) -> usize;

    /// Determines the next available vertex that can be include/exclude
    fn vertex_to_add(&self) -> Option<Self::N>;

    fn nb_nodes(&self) -> usize;

    fn rooted_copy(&self) -> Self;

    fn make_rooted(&mut self);

    fn original_history_len(&self) -> usize;
}

impl DotFormat for UInt32Configuration {

    fn dot_name(&self) -> String {
        let name = |s| {
            match s {
                Included => "i",
                _ => "e"
            }
        };
        return "node_".to_owned() + &c![name(*h), for h in &self.original_history].join("");
    }

    fn to_dot_string(&self) -> String {
        let nb_nodes = self.graph.nb_nodes();
        let vertices = c![v as u32, for v in 0..nb_nodes];
        
        let header_str = 
            "graph {\n\tmargin=0;\n\tnode [fixedsize=true, width=0.3, height=0.3, style=filled];".to_owned();
        
        let body_str: Vec<String> = c!["\t".to_owned() + &self.vertex_to_dot_string(*v), for v in &vertices];

        let edges_str = c!["\t".to_owned() + &self.edge_to_dot_string((*v1,*v2)), 
            for v2 in self.graph.neighbors(*v1).unwrap(),
            for v1 in &vertices,
            if *v1 < *v2].join("\n");

        let footer_str = "}".to_owned();
        return vec![header_str, body_str.join("\n"), edges_str, footer_str].join("\n");
    }
}

impl DotFormat for UInt32ConfigurationSym {
    fn to_dot_string(&self) -> String {
        self.configuration.to_dot_string()
    }

    fn dot_name(&self) -> String {
        self.configuration.dot_name()
    }
}

impl Configuration for UInt32Configuration {
    type N = u32;

    fn new(graph: Arc<Graph>, ms: MixedStrategies) -> UInt32Configuration {
        let vertex_status = vec![(NotSeen,Nil); graph.nb_nodes()];
        let mut vertex_neighbors: Vec<Vec<u32>> = Vec::with_capacity(graph.nb_nodes());
        for n in graph.nodes().iter() {
            vertex_neighbors.push(graph.neighbors(*n).unwrap().clone());
        }
        UInt32Configuration {
            vertex_status: vertex_status,
            subtree_vertices: Vec::new(),
            num_leaves: 0,
            num_excluded: 0,
            border_size: 0,
            subtree_size: 0,
            history: Vec::new(),
            mixed_strategy: ms,
            graph: graph,
            original_history: vec![],
        }
    }
    
    fn backtrack(&mut self) -> (bool, u32) {
        let mut step_back = 0;
        while self.history_len() > 0 {
            let (_, status) = self.undo();
            step_back += 1;
            if status == Status::Included {
                return (true, step_back);    // There's still path to discover
            }
        }
        return (false, step_back);   // No more paths to discover
    }
    
    fn include_vertex(&mut self, v: u32) -> u32 {
        debug_assert!(!self.is_status(&v, Included));
        info!("including: {}", v);
        let mut degree = 0;

        let (old_vertex_status, _) = self.vertex_status[v as usize];

        let neighbors = self.graph.neighbors(v).unwrap().to_vec();
        for i in 0..neighbors.len() {
            let neighbor = neighbors[i];
            let status = self.vertex_status.get(neighbor as usize).unwrap().clone();
            match status {
                (NotSeen, _) => {
                    self.set_status(neighbor, (Border, Nil));
                },  
                (Border, _) => {
                    self.set_status(neighbor, (Excluded, Vertex(v)));
                },
                (Included, Degree(d)) => {
                    degree = d + 1;
                    self.set_status(neighbor, (Included,  Degree(d + 1)));
                    if d == 1 {
                        self.num_leaves -= 1;
                    }
                },
                _ => ()
            }
        }
        let mut d = 0;
        if old_vertex_status == Border {
            d = 1;
        }
        self.set_status(v, (Included, Degree(d)));
        self.subtree_vertices.push(v);
        self.num_leaves += 1;
        self.subtree_size += 1;
        self.history.push((v, Included));
        self.original_history.push(Included);
        debug_assert!(self.debug_border_valid());
        debug_assert!(self.debug_has_valid_info());
        debug_assert!(self.debug_nb_leaves() == self.num_leaves);
        degree
    }
     
    fn exclude_vertex(&mut self, v: u32) {
        debug_assert!(self.is_status(&v, Border) ||
                self.subtree_size == 0);
        info!("excluding: {}", v);
        self.vertex_status[v as usize] = (Excluded, Vertex(v));
        if self.subtree_size > 0 {
            self.border_size -= 1;
        }
        self.num_excluded += 1;
        self.history.push((v, Excluded));
        self.original_history.push(Excluded);
        debug_assert!(self.debug_border_valid());
        debug_assert!(self.debug_has_valid_info());
        debug_assert!(self.debug_nb_leaves() == self.num_leaves);
    }
    
    fn undo(&mut self) -> (u32, Status) {
        assert!(self.history.len() > 0);
        self.original_history.pop();
        let (v, status) = self.history.pop().unwrap();
        if status == Included {
            self.undo_inclusion(v);
        } else {
            self.undo_exclusion(v);
        }
        debug_assert!(self.debug_border_valid());
        debug_assert!(self.debug_has_valid_info());
        debug_assert!(self.debug_nb_leaves() == self.num_leaves);
        (v, status)
    }

    fn vertex_to_add(&self) -> Option<u32> {
        let nb_nodes = self.graph.nb_nodes();
        if self.subtree_size == 0 {
            for i in 0..nb_nodes {
                if let (NotSeen, _) = self.vertex_status[i] {
                    return Some(i as u32);
                }
            }
        }
        else {
            for i in 0..nb_nodes {
                if let (Border, _) = self.vertex_status[i] {
                    return Some(i as u32);
                }
            }
        }
        None
    }

    fn leaf_potential(&self, i: usize) -> u32 {
        let level = self.num_excluded + self.subtree_size;
        if self.mixed_strategy.bound_range.contains(&level) {
            self.leaf_potential_dist(i)
        } else {
            self.leaf_potential_naive(i)
        }
    }

    fn num_leaves(&self) -> usize {
        if self.subtree_size == 1 {
            0
        } else {
            self.num_leaves
        }
    }

    fn num_excluded(&self) -> usize {
        self.num_excluded
    }

    fn subtree_size(&self) -> usize {
        self.subtree_size
    }

    fn nb_nodes(&self) -> usize {
        self.graph.nb_nodes()
    }

    fn num_border(&self) -> usize {
        self.border_size
    }

    fn rooted_copy(&self) -> UInt32Configuration {
        let mut clone = self.clone();
        clone.history.clear();
        return clone;
    }

    fn make_rooted(&mut self) {
        self.history.clear();
    }

    fn original_history_len(&self) -> usize {
        self.original_history.len()
    }

}

impl UInt32Configuration {
    #[allow(dead_code)]

    /// Returns the number of operations that has been made on the configuration
    fn history_len(&self) -> usize {
        self.history.len()
    }

    fn is_status(&self, v: &u32, status: Status) -> bool {
        let (s, _) = self.vertex_status[*v as usize];
        s == status
    }
   
    /// Undo the last operation assuming in the was an inclusion
    /// 
    /// # Arguments
    /// 
    /// * `v` - The vertex to undo the inclusion on
    /// 
    fn undo_inclusion(&mut self, v: u32) {
        let neighbors = self.graph.neighbors(v).unwrap().to_vec();
        for i in 0..neighbors.len() {
            let neighbor = neighbors[i];
            let status = self.vertex_status.get(neighbor as usize).unwrap().clone();
            match status {
                (Border, _) => {
                    self.set_status(neighbor, (NotSeen, Nil));
                }, 
                (Excluded, Vertex(v1)) if v1 == v => {
                    self.set_status(neighbor, (Border, Nil));
                },
                (Included, Degree(d)) => {
                    if d == 2 {
                        self.num_leaves += 1;
                    }
                    if let Some((Included, Degree(d))) = self.vertex_status.get_mut(neighbor as usize) {
                        *d -= 1;
                    }
                },
                _ => continue
            }
        }
        self.subtree_size -= 1;
        if self.subtree_size > 0 {
            self.set_status(v, (Border, Nil))
        } else {
            self.set_status(v, (NotSeen, Nil))
        }
        
        self.num_leaves -= 1;
        self.subtree_vertices.pop();
        debug_assert!(self.debug_border_valid());
        debug_assert!(self.debug_has_valid_info());
        debug_assert!(self.debug_nb_leaves() == self.num_leaves);
    }

    
    /// Undo the last operation assuming in the was an exclusion
    /// 
    /// # Arguments
    /// 
    /// * `v` - The vertex to undo the exclusion on
    ///
    fn undo_exclusion(&mut self, v: u32) {
        if self.subtree_size() == 0 {
            self.set_status(v, (NotSeen, Nil));
        } else {
            self.set_status(v, (Border, Nil));
        }
        debug_assert!(self.debug_border_valid());
        debug_assert!(self.debug_has_valid_info());
        debug_assert!(self.debug_nb_leaves() == self.num_leaves);
    }

    #[allow(dead_code)]
    fn partition_by_distance(&self) -> Vec< Vec<(u32, u32)> >{
        assert!(self.subtree_size > 2);
        
        let mut visited = BTreeSet::<u32>::new();
        let mut queue = VecDeque::<(u32, u32)>::new();
        
        for u in self.subtree_vertices.iter() {
            if let (Included, Degree(d)) = self.vertex_status[*u as usize] {
                if d > 1 {
                    queue.push_back((*u, d));
                }
            }
        }

        let mut layer = vec![];
        let mut vertices = vec![];
        let mut prev_dist = 0;

        while !queue.is_empty() {
            let (v, dist) = queue.pop_front().unwrap();
            if !visited.contains(&v) {
                visited.insert(v);
                if prev_dist < dist {
                    if prev_dist > 0 {
                        vertices.push(layer.clone());
                    }
                    layer.clear();
                }
                let mut degree = 0;
                for u in self.graph.neighbors(v).unwrap().iter() {
                    let (status, _) = self.vertex_status[*u as usize];
                    if status != Excluded {
                        degree += 1;
                        if !visited.contains(u) {
                            queue.push_back((*u, dist + 1));
                        }
                    }
                }
                layer.push((v, degree));
                prev_dist = dist;
            }
        }
        vertices.push(layer);
        vertices
    }

    fn leaf_potential_dist(&self, i: usize) -> u32 {
        let mut current_size = self.subtree_size;
        let mut current_leaf = self.num_leaves;
        let mut lp_dist_dict = HashMap::<usize, usize>::new();
        lp_dist_dict.insert(current_size, current_leaf);

        let vertices_by_dist = self.partition_by_distance();
        for (v,_) in &vertices_by_dist[0] {
            if self.vertex_status[*v as usize].0 == Border {
                current_size += 1;
                current_leaf += 1;
                lp_dist_dict.insert(current_size, current_leaf);
            }
        }

        let max_size = current_size + vertices_by_dist[1..].iter().fold(0, |acc,layer| acc + layer.len());
        let mut current_dist = 1;
        let priority_queue: Vec<(u32, u32)> = vertices_by_dist[0].iter().map(|(u,d)| (*d,*u)).collect();
        let mut priority_queue = BinaryHeap::from(priority_queue);

        while current_size < max_size && !priority_queue.is_empty() {
            let (d,_) = priority_queue.pop().unwrap();

            if current_dist < vertices_by_dist.len() {
                for (v,d) in &vertices_by_dist[current_dist] {
                    if *d > 1 {
                        priority_queue.push((*d, *v));
                    }
                }
                current_dist += 1;
            }
            current_dist -= 1;
            let leaf_to_add = min(d - 1, (max_size-current_size) as u32);
            for _ in 0..leaf_to_add {
                current_size += 1;
                current_leaf += 1;
                lp_dist_dict.insert(current_size, current_leaf);
            }
        }
        if lp_dist_dict.contains_key(&i) {
            return lp_dist_dict[&i] as u32;
        }
        return 0;
    }

    fn leaf_potential_naive(&self, i: usize) -> u32 {
        if i > self.subtree_size + self.border_size {
            return (self.num_leaves + i - self.subtree_size - 1) as u32;
        }
        return (self.num_leaves + i - self.subtree_size) as u32;
    }

    fn vertex_to_dot_string(&self, v: u32) -> String {
        let color;
        match self.vertex_status.get(v as usize).unwrap().0 {
            Included => color = "green",
            Excluded => color = "red",
            NotSeen => color = "blue",
            Border => color = "yellow"
        }
        return format!("\"{}\" [label=\"{}\", fillcolor={}];", v, v, color);
    }

    fn edge_to_dot_string(&self, edge: (u32, u32)) -> String {
        let (v1,v2) = edge;
        return format!("\"{}\" -- \"{}\";", v1, v2);
    }    

    fn last_operation(&self) -> Option<(u32, Status)> {
        if self.history_len() == 0 {
            return None;
        }
        return Some(*self.history.last().unwrap());
    }

    fn set_status(&mut self, v: u32, tuple: (Status, StatusInfo<u32>)) {
        assert!((v as usize) < self.nb_nodes());
        if v == 11 {
            let _debug1 = 1;
        }
        let current_status = self.vertex_status[v as usize];
        match current_status.0 {
            Border => self.border_size -= 1,
            Excluded => self.num_excluded -= 1,
            _ => ()
        }
        let (new_status, _) = tuple;
        match new_status {
            Border => self.border_size += 1,
            Excluded => self.num_excluded += 1,
            _ => ()
        }
        self.vertex_status[v as usize] = tuple;
    }

    /// Debug only (in debug_assert!)
    fn debug_nb_leaves(&self) -> usize {
        let subtree = self.subtree_vertices.to_vec();
        if subtree.len() == 1 {
            return 1;
        }
        return subtree.iter()
            .filter(|v| {
                let nb = self.graph.neighbors(**v).unwrap().to_vec();
                let y = nb.iter().fold(0, |acc, x| {
                    if self.vertex_status[*x as usize].0 == Included {
                        return acc + 1;
                    }
                    return acc;
                });
                return y == 1;
            })
            .count()
    }

    /// Debug only (in debug_assert!)
    fn debug_has_valid_info(&self) -> bool {
        let subtree = self.subtree_vertices.to_vec();
        return subtree.iter()
            .all(|v| {
                let nb = self.graph.neighbors(*v).unwrap().to_vec();
                let y = nb.iter().fold(0, |acc, x| {
                    if self.vertex_status[*x as usize].0 == Included {
                        return acc + 1;
                    }
                    return acc;
                });
                return self.vertex_status[*v as usize] == (Included, Degree(y));
            });
    }

    fn debug_border_valid(&self) -> bool {
        return self.vertex_status.iter()
            .enumerate()
            .filter(|(_, (status, _))| *status == Border)
            .all(|(v, _)| {
                let nb = self.graph.neighbors(v as u32).unwrap().to_vec();
                return nb.iter().any(|n| self.vertex_status[*n as usize].0 == Included);
            });
    }
}

impl Configuration for UInt32ConfigurationSym {
    type N = u32;
    fn new(graph: Arc<Graph>, ms: MixedStrategies) -> UInt32ConfigurationSym {
        UInt32ConfigurationSym {
            configuration: UInt32Configuration::new(graph, ms),
            forbidden_patterns: vec![],
            excluded_history: vec![],
            symmetrie_level: 0,
        }
    }

    fn include_vertex(&mut self, v: u32) -> u32 {
        self.symmetrie_level += 1;
        let degree = self.configuration.include_vertex(v);
        if self.mixed_strategy().symmetry_range.contains(&self.symmetrie_level) {
            self.exclude_excluables(v);
        }
        return degree;
    }

    fn exclude_vertex(&mut self, v: u32) {
        self.symmetrie_level += 1;
        self.configuration.exclude_vertex(v);
        if self.mixed_strategy().symmetry_range.contains(&self.symmetrie_level) {
            let mut tree: Vec<u32> = self.configuration.subtree_vertices.to_vec();
            tree.push(v);
            self.forbidden_patterns = self.graph().orbit_from(&tree);
            self.exclude_excluables(v);
        }
    }

    fn undo(&mut self) -> (u32, Status) {
        assert!(self.configuration.history_len() > 0);
        if self.mixed_strategy().symmetry_range.contains(&self.symmetrie_level) {
            let last_op = self.configuration.last_operation().unwrap();
            if last_op.1 == Excluded {
                self.forbidden_patterns.clear();
            }
            self.undo_last_exclusion_batch();
        }
        self.symmetrie_level -= 1;
        return self.configuration.undo();
    }

    fn backtrack(&mut self) -> (bool, u32) {
        let mut step_back = 0;
        while self.configuration.history_len() > 0 {
            let (_u, status) = self.undo();
            step_back += 1;
            if status == Status::Included {
                return (true, step_back);    // There's still path to discover
            }
        }
        return (false, step_back);   // No more paths to discover
    }

    fn leaf_potential(&self, i: usize) -> u32 {
        self.configuration.leaf_potential(i)
    }
    
    fn num_leaves(&self) -> usize {
        self.configuration.num_leaves()
    }

    fn num_excluded(&self) -> usize {
        self.configuration.num_excluded()
    }

    fn subtree_size(&self) -> usize {
        self.configuration.subtree_size()
    }

    fn num_border(&self) -> usize {
        self.configuration.border_size
    }

    /// Determines the next available vertex that can be include/exclude
    fn vertex_to_add(&self) -> Option<u32> {
        self.configuration.vertex_to_add()
    }

    fn nb_nodes(&self) -> usize {
        self.configuration.nb_nodes()
    }

    fn rooted_copy(&self) -> UInt32ConfigurationSym {
        let mut clone = self.clone();
        clone.configuration.history.clear();
        return clone;
    }

    fn make_rooted(&mut self) {
        self.configuration.history.clear();
    }

    fn original_history_len(&self) -> usize {
        self.configuration.original_history_len()
    }
}

impl UInt32ConfigurationSym {

    fn mixed_strategy(&self) -> &MixedStrategies {
        &self.configuration.mixed_strategy
    }

    fn graph(&self) -> &Graph {
        &self.configuration.graph
    }

    fn set_status(&mut self, v: u32, status: (Status, StatusInfo<u32>)) {
        self.configuration.set_status(v, status);
    }

    fn vertex_status(&self, v: u32) -> Status {
        self.vertex_status_with_info(v).0
    }

    fn vertex_status_with_info(&self, v: u32) -> (Status, StatusInfo<u32>) {
        assert!((v as usize) < self.configuration.vertex_status.len());
        self.configuration.vertex_status[v as usize].clone()
    }
    
    fn left_outer(&self, left: &Vec<u32>, right: &Vec<u32>) -> Vec<u32> {
        let mut visited = HashSet::with_capacity(right.len());
        right.iter().for_each(|u| { visited.insert(*u); });
        return left.iter()
            .filter(|v| !visited.contains(v))
            .map(|v| *v)
            .collect();
    }

    fn excluable_vertices(&self) -> Vec<u32> {
        let subtree = self.configuration.subtree_vertices.to_vec();
        let mut excluable = vec![];
        let mut visited = vec![false;self.nb_nodes()];
        for pattern in self.forbidden_patterns.iter() {
            let mut unmarked = self.left_outer(pattern, &subtree);
            if unmarked.len() > 1 {
                continue;
            }
            let u = unmarked.pop().unwrap();
            if !visited[u as usize] {
                visited[u as usize] = true;
                match self.vertex_status(u) {
                    NotSeen | Border => excluable.push(u),
                    _ => ()
                }
            }
        }
        assert!(excluable.len() < self.nb_nodes());
        debug_assert!(excluable.iter().all(|n| !subtree.contains(n)));
        debug_assert!(excluable.iter().all(|n| 
            self.vertex_status(*n) != Excluded 
            && self.vertex_status(*n) != Included));
        excluable
    }

    fn exclude_excluables(&mut self, v: u32) -> usize {
        let excluable = self.excluable_vertices();
        let mut save = Vec::with_capacity(excluable.len());        
        excluable.iter().for_each(|u| {
            save.push((*u, self.vertex_status_with_info(*u)));
            self.set_status(*u, (Excluded, Vertex(v)));
        });
        info!("vertex: {}, excludes: {:?}", v, excluable);
        self.excluded_history.push(save);
        debug_assert!(excluable.iter().all(|n| {
            match self.vertex_status_with_info(*n) {
                (Excluded, Vertex(u)) => u == v,
                _ => false
            }
        }));
        excluable.len()
    }

    /// Undo all exclusion done by `v`.
    fn undo_last_exclusion_batch(&mut self) {
        let last_batch = self.excluded_history.pop().unwrap();
        last_batch.iter()
            .for_each(|(u, status_winfo)| self.set_status(*u, *status_winfo));
        debug_assert!(self.configuration.debug_has_valid_info());
    }
}

#[cfg(test)]
mod tests {
    use configuration::*;
    use range_expr_parser::{no_range};
    use std::ops::{Bound};

    fn simplify_status_u32_conf(c: &UInt32Configuration) -> Vec<(u32, Status)> {
        let mut vec = Vec::<(u32,Status)>::new();
        let mut v = 0;
        for (status, _) in c.vertex_status.iter() {
            vec.push((v, *status));
            v += 1;
        }
        vec.sort_by(|(x,_), (y,_)| x.cmp(y));
        vec
    }
    
    fn build_mixed_strategy() -> MixedStrategies {
        MixedStrategies {
            bound_range: no_range(),
            symmetry_range: no_range(),
        }
    }

    fn build_mixed_strategy_sym(sym: usize) -> MixedStrategies {
        MixedStrategies {
            bound_range: no_range(),
            symmetry_range: (Bound::Unbounded, Bound::Excluded(sym)),
        }
    }

    #[test]
    fn test_inclusion_hypercube3_sym() {
        let g = Arc::new(Graph::hyper_cube(3));
        let mut c = UInt32ConfigurationSym::new(g, build_mixed_strategy_sym(10));
        c.include_vertex(0);
        assert_eq!(0, c.num_excluded());
    }

    #[test]
    fn test_dot_string_hypercube4_sym() {
        let g = Arc::new(Graph::hyper_cube(3));
        let mut c = UInt32ConfigurationSym::new(g, build_mixed_strategy_sym(10));
        c.include_vertex(0);
        println!("{}", c.to_dot_string());
        assert_eq!(0, c.num_excluded());
    }

    #[test]
    fn test4_uint32_configuration() {
        let g = Arc::new(Graph::complete_graph(4));
        let mut c = UInt32Configuration::new(g, build_mixed_strategy());
        c.include_vertex(0);
        c.include_vertex(1);
        let expected = vec![(0, Included), (1, Included), (2, Excluded), (3, Excluded)];
        assert_eq!(expected, simplify_status_u32_conf(&c));
    }


    #[test]
    fn test5_uint32_configuration() {
        let g = Arc::new(Graph::complete_graph(4));
        let mut c = UInt32Configuration::new(g, build_mixed_strategy());
        c.exclude_vertex(0);
        let expected = vec![(0, Excluded), (1, NotSeen), (2, NotSeen), (3, NotSeen)];
        assert_eq!(expected, simplify_status_u32_conf(&c));
    }


    #[test]
    fn test6_uint32_configuration() {
        let g = Arc::new(Graph::complete_graph(4));
        let mut c = UInt32Configuration::new(g, build_mixed_strategy());
        c.exclude_vertex(0);
        c.exclude_vertex(1);
        c.exclude_vertex(2);
        assert_eq!(Some(3), c.vertex_to_add());
    }

    #[test]
    fn test7_uint32_configuration() {
        let g = Arc::new(Graph::complete_graph(4));
        let mut c = UInt32Configuration::new(g, build_mixed_strategy());
        c.include_vertex(0);
        c.include_vertex(1);
        c.undo();
        let expected = vec![(0, Included), (1, Border), (2, Border), (3, Border)];
        assert_eq!(expected, simplify_status_u32_conf(&c));
    }

    #[test]
    fn test_vertex_dot_string1() {
        let g = Arc::new(Graph::complete_graph(10));
        let mut c = UInt32Configuration::new(g, build_mixed_strategy());
        c.include_vertex(0);
        let expected = "\"0\" [label=\"0\", fillcolor=green];".to_owned();
        let actual = c.vertex_to_dot_string(0);
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_resolve_dot_name_empty() {
        let g = Arc::new(Graph::complete_graph(10));
        let c = UInt32Configuration::new(g, build_mixed_strategy());
        let expected = "node_";
        let actual = c.dot_name();
        assert_eq!(expected, actual);
    }

    #[test]
    fn test_resolve_dot_name_iie() {
        let g = Arc::new(Graph::wheel_graph(10));
        let mut c = UInt32Configuration::new(g, build_mixed_strategy());
        c.include_vertex(0);
        c.include_vertex(1);
        c.exclude_vertex(2);
        let expected = "node_iie";
        let actual = c.dot_name();
        assert_eq!(expected, actual);
    }
}
