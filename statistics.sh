#!/bin/bash

sum() {
    O_IFS=$O_IFS
    IFS=$(echo -en "\n\b")
    sum=0
    for LINE in $1[@]; do
        num=`egrep -o "[0-9]+" <<< "$LINE"`
        sum=`expr $num + $sum`
    done
    IFS=$O_IFS
    echo $sum
}

avg() {
    echo `bc <<< "scale=10; $1 / $2"`
}


exec_target() {
    FILENAME="$1_temp.txt"
    make "$1" > $FILENAME   
    num_leaf=`grep -c "new leaf" "$FILENAME"`
    VERTEX_TO_LEAVES=`egrep -o "[0-9]+ -> [0-9]+" "$FILENAME" | sort | uniq -c`
    configuration_tried=`grep -c "trying a configuration" "$FILENAME"`
    num_include=`grep -c "including" "$FILENAME"`
    num_exclude=`grep -c "excluding" "$FILENAME"`
    num_backtrack=`grep -c "backtracking" "$FILENAME"`
    backtrack_steps=`egrep -o "backtracked [0-9]+ steps" "$FILENAME"`
    
    lp_tries=`egrep -o "called leaf potential: [0-9]+ times before promising" $FILENAME`
    sum_lp_tries=$( sum "$lp_tries")
    lp_tries_occurence=`grep -c "called leaf potential" "$FILENAME"`
    backtrack_step_occurence=`egrep -c "backtracked [0-9]+ steps" "$FILENAME"`
    
    steps_sum=$( sum "$backtrack_steps" )
    avg_steps=$( avg "$steps_sum" "$backtrack_step_occurence" )
    avg_lp_tries=$( avg "$sum_lp_tries" "$lp_tries_occurence" )
    
    echo "number of leaves visited: "$num_leaf""
    echo "number of configuration tried: "$configuration_tried""
    echo "Total inclusion: "$num_include""  
    echo "Total exclusion: "$num_exclude""
    echo "Total backtrack: "$num_backtrack""   
    echo "Total backtrack steps: "$steps_sum""
    echo "Average backtrack steps: "$avg_steps""
    echo "Total leaf potential tries: "$sum_lp_tries""
    echo "Average leaf potential tries: "$avg_lp_tries""
}

make release

exec_target $1
echo done