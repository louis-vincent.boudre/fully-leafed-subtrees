EXEC = fully-leafed-subtrees
BIN_FOLDER = target/release

.PHONY: clean test hypercube5 complete10 wheel11 bipartite_7_5 cycle10

bench: release
	./$(BIN_FOLDER)/$(EXEC) -B

snark: release
	./$(BIN_FOLDER)/$(EXEC) -g snark -v $(n) -S ..10 -t $(t)

hoffman: release
	./$(BIN_FOLDER)/$(EXEC) -g hoffman-singleton -S ..20 -t $(t)

friendship: release
	./$(BIN_FOLDER)/$(EXEC) -g friendship -v $(n) -t $(t) -S ..6

generalized-peterson: release
	./$(BIN_FOLDER)/$(EXEC) -g generalized-peterson -v $(n) -u $(k) -t $(t) -S $(s)

h4_naive: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 4 -t 8

h3_sym: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 3 -S ..10 -G ..10

h3_sym_dist: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 3 -S ..10 -V -b ..

h4_sym: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 4 -S ..10 -V

h4_sym_dist: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 4 -S ..10 -b .. -V

h5_sym: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 5 -S ..10 -t $(t)

h5_sym_verbose: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 5 -S ..10 -V

h6_sym: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 6 -S ..10 -t $(t)

h6_naive: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 6 -t 8

h6_dist: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 6 -t 8 -b ..

h5_naive: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 5 -t 8

h5_dist: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 5 -t 8 -b ..

random40_naive: release
	./$(BIN_FOLDER)/$(EXEC) -g random -v 40 -d 0.2 -t 8 -c 2

random40_dist: release
	./$(BIN_FOLDER)/$(EXEC) -g random -v 40 -d 0.2 -b .. -t 8 -c 2

random30_naive: release
	./$(BIN_FOLDER)/$(EXEC) -g random -v 30 -d 0.2 -t 8 -c 2

random30_dist: release
	./$(BIN_FOLDER)/$(EXEC) -g random -v 30 -d 0.2 -b .. -t 8 -c 2

random20_naive: release
	./$(BIN_FOLDER)/$(EXEC) -g random -v 20 -d 0.2 -t 8 -c 2

random20_dist: release
	./$(BIN_FOLDER)/$(EXEC) -g random -v 20 -d 0.2 --bound .. -t 8 -c 2

random20_dist2: release
	./$(BIN_FOLDER)/$(EXEC) -g random -v 20 -d 0.2 -b ..10 -t 8 -c 2

cycle10: release
	./$(BIN_FOLDER)/$(EXEC) -g cycle

cycle4: release
	./$(BIN_FOLDER)/$(EXEC) -g cycle -v 4

bipartite_7_5: release
	./$(BIN_FOLDER)/$(EXEC) -g bipartite -v 7 -u 5

wheel11: release
	./$(BIN_FOLDER)/$(EXEC) -g wheel -v 11

complete10: release
	./$(BIN_FOLDER)/$(EXEC) -g complete -v 10

hypercube5: release
	./$(BIN_FOLDER)/$(EXEC) -g hypercube -D 5 -t 8

release:
	cargo +nightly build --release 

debug:
	cargo +nightly build

test:
	cargo +nightly test

clean:
	rm -fr target/
