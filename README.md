# Fully-leafed induced subtrees

This repository contains code to compute the leaf function of a graph. It is
based on a Python implementation depending on the
[Sagemath](http://www.sagemath.org/) software available on [this repository](https://github.com/enadeau/fully-leafed-induced-subtrees). An
article available on [arXiv]((https://arxiv.org/abs/1709.09808) explains
the main ideas.

We decided to implement this alternative version in Rust instead of Python. The
main objectif is cut the search-space by exploiting the graph symmetries and using
multi-threading.

Below are instructions to get you a copy of the project up and running on your
local machine for development and testing purposes.

### Prerequisites

- [Rust-lang](https://www.rust-lang.org/en-US/install.html), compiler used for
  the project (version 1.26.1/2018-05-25);
- [Cargo](https://crates.io/), the package manager and project build;
- [Rustup](https://rustup.rs/), manage different Rust versions/channels.

*Note*: Cargo and Rustup gets installed automatically if you follow the first
the instructions in the [Rust official
documentation](https://www.rust-lang.org/en-US/install.html).

### Installing

The project needs to run on [Nightly Rust](https://doc.rust-lang.org/1.15.1/book/nightly-rust.html). 
This version allow us to use unstable feature for benchmarking, testing and useful modules.

To install the nightly version, type

```bash
$ rustup install nightly
```

### Building and running

Here is the command to build the project without any optimization:

```bash
$ cargo +nightly build
```

To build the project with all the optimizations :

```bash
$ cargo +nightly build --release
```

Finally you can run the project in the same fashion, only changing `build` for
`run`, like so:

```bash
$ cargo +nightly run
```

And in release mode:

```bash
$ cargo +nightly run --release
```

*Note*: The first time you build the project, it will download/compile all the
dependencies you need.  You can look at the [`Cargo.toml`](./Cargo.toml) file
to see the dependencies of the project.

## How it works

The program calculates the `leaf function` of a graph. Here's a preview of all
the options you can specify:

```bash
Usage: ./target/release/fully-leafed-subtrees 
        [--help] [--verbose] 
        [--graph graph-type] 
        [--num-vertices number] 
        [--num-vertices2 number]
        [--density [0.2; 0.8]] 
        [--dimension number] 
        [--num-contexts number] 
        [--num-threads number] 
        [--strategy (naive|dist)]
        [--list-graph]

Options:
    -t, --num-threads number
                        Sets the number of maximum thread. The default value
                        is 0 (singlethreaded)
    -g, --graph [cycle|wheel|complete|bipartite|random|hypercube|snark|hoffman-singleton]
                        Sets the graph to use. The default value is the
                        complete graph.
    -v, --num-vertices number
                        Sets the number of vertices. The default value is 10.
    -u, --num-vertices2 number
                        Second number of vertices, only works for the
                        bipartite graph, otherwise it is ignored. The default
                        value is 10.
    -d, --density [0.2; 0.8]
                        Sets the density of a graph. This argument works only
                        on random graph, otherwise ignored. The default value
                        is 0.8.
    -D, --dimension number
                        Sets the dimension of an hypercube. This argument
                        works only on hypecube graph, otherwise ignored. The
                        default value is 3
    -G, --graphviz range (a..b|a..|..b|a|..)
                        Generate `tree.png` to a certain depth level provided
                        in argument.
    -S, --symmetries range (a..b|a..|..b|a|..)
                        Avoid symmetries while calculating leaf map.
    -b, --bound range (a..b|a..|..b|a|..)
                        Use a bound strategy to calculate the leaf potential.
                        When the bound is set to 0 it is ignored, when it's
                        set to -1 is the only bound strategy used, when it's
                        set to any number > 0, it will be the threshold until
                        the configuration goes back to the naive leaf
                        potential function. Example: ./fully-leafed -g custom
                        -v 20 -b 10, will use the bound strategy until its
                        number of excluded vertices + the size of its subtree
                        is equal 10, after that it fallback to the naive
                        approche.
    -f, --fixed number  Fixed the subtree size
    -l, --list-graph    Lists the entire graph that can be pass to --graph
                        option
    -B, --benchmark     Execute a benchmark on multiple graph. Ignores
                        everyother arguments.
    -h, --help          print this help menu
    -V, --verbose       prints information messages in the console during the
                        execution
```

By default, the command `./target/release/fully-leafed-subtrees` will print the
help shown above. Here an example of how to solve the leaf function for a wheel
graph of 11 + 1 vertices:

```bash
$ ./target/release/fully-leafed-subtrees --graph wheel --num-vertices 11

Result for wheel graph : [0, 0, 2, 2, 3, 4, 5, 2, 2, 2, 2, -2147483648, -2147483648], in 0ms
```

Another example for the complete bipartite graph of 7 and 5 vertices :

```bash
$ ./target/release/fully-leafed-subtrees --graph bipartite --num-vertices 7 --num-vertices2 5

Result for bipartite graph : [0, 0, 2, 2, 3, 4, 5, 6, 7, -2147483648, -2147483648, -2147483648, -2147483648], in 0ms
```

To solve the hypercube graph of fifth dimension with 8 threads and 4 contexts
per thread:

```bash
$ ./target/release/fully-leafed-subtrees --graph hypercube --dimension 5 --num-threads 8

Result for hypercube graph : [0, 0, 2, 2, 3, 4, 5, 4, 5, 6, 6, 6, 7, 7, 7, 8, 8, 8, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648, -2147483648], in 353ms
```

## Running the tests

To run the tests, simply use this command:

```bash
$ cargo +nightly test
```

If you want to see all the `stdout` output (e.g. The println! macros), use this
command instead:

```bash
$ cargo +nightly test -- --nocapture
```

## Authors

* **Louis-Vincent Boudreault** - *Initial work* -
  [louis-vincent.boudre](https://gitlab.com/louis-vincent.boudre)

## License

This project is licensed under the GNU GPLv3 License - see the
[LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [fully-leafed-induced-subtrees](https://github.com/enadeau/fully-leafed-induced-subtrees),
  by Émile Nadeau, for the base project made in Python with the main
  algorithms.
* [Fully leafed induced subtrees](https://arxiv.org/abs/1709.09808), article
  explaining the theory and main ideas.
